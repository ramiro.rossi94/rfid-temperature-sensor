#!/bin/bash

echo "################# Google-SkyWater 130nm PDK and Tools Setup             #################"

PDK_TOOLS_FOLDER="$HOME/skywater"
PROJECTS_FOLDER="$HOME/Projects"
TIMESTAMP=$(date +'%Y-%m-%d-%2H%2M%2S')

if [ ! -d "$PDK_TOOLS_FOLDER" ]; then
	echo "Creating directory for PDK & Tools: \"$PDK_TOOLS_FOLDER\"..."

		mkdir -p "$PDK_TOOLS_FOLDER/install-logs"
		cd $PDK_TOOLS_FOLDER
fi

if [ ! -d "$PROJECTS_FOLDER" ]; then
	echo "Creating directory for working Projects: \"$PROJECTS_FOLDER\"..."

		mkdir -p "$PROJECTS_FOLDER"
fi

read -p "Install dependencies? [y/n]" -n 1 -r
echo
if [[ $REPLY =~ ^[Yy]$ ]]; then
	echo "Installing dependencies..."
		sudo apt-get update
		sudo apt-get install curl git make tcllib libtool automake xterm libomp-dev \
				                 autoconf texinfo libreadline-dev tcl8.6-dev tk8.6-dev \
				                 libx11-dev libxaw7-dev libxrender-dev libx11-xcb-dev \
				                 libxpm-dev bison flex libcairo2-dev m4 tcsh \
				                 csh tcl-dev tk-dev ca-certificates qt5-default \
				                 libqt5designer5 libqt5multimedia5 libqt5multimediawidgets5 \
				                 libqt5opengl5 libqt5svg5 libqt5xmlpatterns5 libruby
fi

read -p "Install docker? [y/n]" -n 1 -r
echo
if [[ $REPLY =~ ^[Yy]$ ]]; then
	echo "Installing docker..."
		sudo snap install docker
		sudo groupadd docker
		sudo usermod -aG docker $USER
		echo "You need to restart to continue installation"
		exit
fi

read -p "Install Google-Skywater 130nm PDK? [y/n]" -n 1 -r
echo
if [[ $REPLY =~ ^[Yy]$ ]]; then
	echo "Installing Google-Skywater 130nm PDK..."
		cd $PDK_TOOLS_FOLDER
		LOG_FILE="$PDK_TOOLS_FOLDER/install-logs/skywater-pdk_$TIMESTAMP.log"
		touch $LOG_FILE
		echo "################# Cloning main repo #################" >> $LOG_FILE
		git clone https://github.com/google/skywater-pdk.git &>> $LOG_FILE
		cd skywater-pdk >> $LOG_FILE
		echo "################# Cloning submodule sky130_fd_io #################" >> $LOG_FILE
		git submodule init libraries/sky130_fd_io/latest &>> $LOG_FILE
		echo "################# Cloning submodule sky130_fd_pr #################" >> $LOG_FILE
		git submodule init libraries/sky130_fd_pr/latest &>> $LOG_FILE
		echo "################# Cloning submodule sky130_fd_sc_hd #################" >> $LOG_FILE
		git submodule init libraries/sky130_fd_sc_hd/latest &>> $LOG_FILE
		echo "################# Cloning submodule sky130_fd_sc_hdll #################" >> $LOG_FILE
		git submodule init libraries/sky130_fd_sc_hdll/latest &>> $LOG_FILE
		echo "################# Cloning submodule sky130_fd_sc_hs #################" >> $LOG_FILE
		git submodule init libraries/sky130_fd_sc_hs/latest &>> $LOG_FILE
		echo "################# Cloning submodule sky130_fd_sc_ms #################" >> $LOG_FILE
		git submodule init libraries/sky130_fd_sc_ms/latest &>> $LOG_FILE
		echo "################# Cloning submodule sky130_fd_sc_ls #################" >> $LOG_FILE
		git submodule init libraries/sky130_fd_sc_ls/latest &>> $LOG_FILE
		echo "################# Cloning submodule sky130_fd_sc_lp #################" >> $LOG_FILE
		git submodule init libraries/sky130_fd_sc_lp/latest &>> $LOG_FILE
		echo "################# Cloning submodule sky130_fd_sc_hvl #################" >> $LOG_FILE
		git submodule init libraries/sky130_fd_sc_hvl/latest &>> $LOG_FILE
		echo "################# Updating submodules #################" >> $LOG_FILE
		git submodule update &>> $LOG_FILE
		echo "################# Making project #################" >> $LOG_FILE
		make timing &>> $LOG_FILE
		echo "################# Finished #################" >> $LOG_FILE
fi

read -p "Install symbols for XSchem? [y/n]" -n 1 -r
echo
if [[ $REPLY =~ ^[Yy]$ ]]; then
	echo "Installing Google-Skywater symbols for Xschem..."
		cd $PDK_TOOLS_FOLDER
		LOG_FILE="$PDK_TOOLS_FOLDER/install-logs/xschem_sky130_$TIMESTAMP.log"
		touch $LOG_FILE
		echo "################# Cloning main repo #################" >> $LOG_FILE
		git clone https://github.com/StefanSchippers/xschem_sky130.git &>> $LOG_FILE
		echo "################# Finished #################" >> $LOG_FILE
fi

read -p "Install ngspice? [y/n]" -n 1 -r
echo
if [[ $REPLY =~ ^[Yy]$ ]]; then
	echo "Installing ngspice 33..."
		cd $PDK_TOOLS_FOLDER
		LOG_FILE="$PDK_TOOLS_FOLDER/install-logs/ngspice_$TIMESTAMP.log"
		touch $LOG_FILE
		echo "################# Downloading ngspice #################" >> $LOG_FILE
		wget https://sourceforge.net/projects/ngspice/files/ng-spice-rework/33/ngspice-33.tar.gz &>> $LOG_FILE
		echo "################# Extracting tar #################" >> $LOG_FILE
		tar -zxf ngspice-33.tar.gz &>> $LOG_FILE
		cd ngspice-33 &>> $LOG_FILE
		echo "################# Configuring release #################" >> $LOG_FILE
		mkdir release &>> $LOG_FILE
		cd release &>> $LOG_FILE
		../configure --with-x --enable-xspice --enable-cider --enable-openmp --with-readlines=yes --disable-debug &>> $LOG_FILE
		echo "################# Runnig make of release #################" >> $LOG_FILE
		make &>> $LOG_FILE
		sudo make install &>> $LOG_FILE
		cd ../ &>> $LOG_FILE
		echo "################# Configuring build-lib #################" >> $LOG_FILE
		mkdir build-lib &>> $LOG_FILE
		cd build-lib &>> $LOG_FILE
		../configure --with-x --enable-xspice --enable-cider --enable-openmp --disable-debug --with-ngshared &>> $LOG_FILE
		echo "################# Runnig make of build-lib #################" >> $LOG_FILE
		make &>> $LOG_FILE
		sudo make install &>> $LOG_FILE
		cd ../../ &>> $LOG_FILE
		echo "################# Deleting temp files #################" >> $LOG_FILE
		rm ngspice-33.tar.gz &>> $LOG_FILE
		echo "################# Finished #################" >> $LOG_FILE
fi

read -p "Install XSchem? [y/n]" -n 1 -r
echo
if [[ $REPLY =~ ^[Yy]$ ]]; then
	echo "Installing Xschem..."
		cd $PDK_TOOLS_FOLDER
		LOG_FILE="$PDK_TOOLS_FOLDER/install-logs/xschem_$TIMESTAMP.log"
		touch $LOG_FILE
		echo "################# Cloning main repo #################" >> $LOG_FILE
		git clone https://github.com/StefanSchippers/xschem.git &>> $LOG_FILE
		cd xschem >> $LOG_FILE
		echo "################# Configuring XSchem #################" >> $LOG_FILE
		./configure --prefix=/usr/local --user-conf-dir=~/.xschem \
				        --user-lib-path=~/share/xschem/xschem_library \
				        --sys-lib-path=/usr/local/share/xschem/xschem_library &>> $LOG_FILE
		echo "################# Running make #################" >> $LOG_FILE
		make &>> $LOG_FILE
		sudo make install &>> $LOG_FILE
		echo "################# Creating launcher #################" >> $LOG_FILE
		cd $PDK_TOOLS_FOLDER &>> $LOG_FILE
		touch xschem.sh &>> $LOG_FILE
		echo "cd ~/Projects && xschem" >> xschem.sh
		chmod +x xschem.sh &>> $LOG_FILE
		wget https://i.ibb.co/NykGj2J/xschem.png &>> $LOG_FILE
		XSCHEM_SHORTCUT="$HOME/.local/share/applications/xschem.desktop"
		touch $XSCHEM_SHORTCUT &>> $LOG_FILE
		printf "%s\n" "[Desktop Entry]" >> $XSCHEM_SHORTCUT
		printf "%s\n" "Version=1.0" >> $XSCHEM_SHORTCUT
		printf "%s\n" "Name=XSchem" >> $XSCHEM_SHORTCUT
		printf "%s\n" "Comment=Launch XSchem from Projects folder" >> $XSCHEM_SHORTCUT
		printf "%s\n" "GenericName=XSchem" >> $XSCHEM_SHORTCUT
		printf "%s\n" "Exec=$PDK_TOOLS_FOLDER/xschem.sh" >> $XSCHEM_SHORTCUT
		printf "%s\n" "Terminal=false" >> $XSCHEM_SHORTCUT
		printf "%s\n" "X-MultipleArgs=false" >> $XSCHEM_SHORTCUT
		printf "%s\n" "Type=Application" >> $XSCHEM_SHORTCUT
		printf "%s\n" "Icon=$PDK_TOOLS_FOLDER/xschem.png" >> $XSCHEM_SHORTCUT
		printf "%s\n" "StartupNotify=true" >> $XSCHEM_SHORTCUT
		chmod 755 $XSCHEM_SHORTCUT &>> $LOG_FILE
		echo "################# Creating xschemrc #################" >> $LOG_FILE
		mkdir -p "$PROJECTS_FOLDER" &>> $LOG_FILE
		touch $PROJECTS_FOLDER/xschemrc
		printf "%s\n" "set XSCHEM_LIBRARY_PATH {}" >> $PROJECTS_FOLDER/xschemrc
		printf "%s\n" "append XSCHEM_LIBRARY_PATH \${XSCHEM_SHAREDIR}/xschem_library" >> $PROJECTS_FOLDER/xschemrc
		printf "%s\n" "append XSCHEM_LIBRARY_PATH :$PDK_TOOLS_FOLDER/xschem_sky130" >> $PROJECTS_FOLDER/xschemrc
		printf "%s\n" "append XSCHEM_LIBRARY_PATH :$PROJECTS_FOLDER" >> $PROJECTS_FOLDER/xschemrc
		printf "%s\n" "set dircolor(xschem_library$) red" >> $PROJECTS_FOLDER/xschemrc
		printf "%s\n" "set dircolor(xschem_sky130$) green" >> $PROJECTS_FOLDER/xschemrc
		printf "%s\n" "set SKYWATER_MODELS $PDK_TOOLS_FOLDER/skywater-pdk/libraries/sky130_fd_pr_ngspice/latest" >> $PROJECTS_FOLDER/xschemrc
		printf "%s\n" "set SKYWATER_STDCELLS $PDK_TOOLS_FOLDER/skywater-pdk/libraries/sky130_fd_sc_hd/latest" >> $PROJECTS_FOLDER/xschemrc
		printf "%s\n" "lappend tcl_files \${XSCHEM_SHAREDIR}/ngspice_backannotate.tcl" >> $PROJECTS_FOLDER/xschemrc
		printf "%s\n" "lappend tcl_files $PDK_TOOLS_FOLDER/xschem_sky130/scripts/sky130_models.tcl" >> $PROJECTS_FOLDER/xschemrc
		echo "################# Finished #################" >> $LOG_FILE
fi

read -p "Install Magic? [y/n]" -n 1 -r
echo
if [[ $REPLY =~ ^[Yy]$ ]]; then
	echo "Installing Magic..."
		cd $PDK_TOOLS_FOLDER
		LOG_FILE="$PDK_TOOLS_FOLDER/install-logs/magic_$TIMESTAMP.log"
		touch $LOG_FILE
		echo "################# Cloning main repo #################" >> $LOG_FILE
		git clone git://opencircuitdesign.com/magic &>> $LOG_FILE
		cd magic &>> $LOG_FILE
		echo "################# Checkout of version 8.3 #################" >> $LOG_FILE
		git checkout magic-8.3 &>> $LOG_FILE
		echo "################# Configuring Magic #################" >> $LOG_FILE
		./configure &>> $LOG_FILE
		echo "################# Running make #################" >> $LOG_FILE
		make &>> $LOG_FILE
		sudo make install &>> $LOG_FILE
		echo "################# Finished #################" >> $LOG_FILE
fi

read -p "Install KLayout? [y/n]" -n 1 -r
echo
if [[ $REPLY =~ ^[Yy]$ ]]; then
	echo "Installing klayout..."
		cd $PDK_TOOLS_FOLDER
		LOG_FILE="$PDK_TOOLS_FOLDER/install-logs/klayout_$TIMESTAMP.log"
		touch $LOG_FILE
		echo "################# Downloading KLayout #################" >> $LOG_FILE
		wget https://www.klayout.org/downloads/Ubuntu-20/klayout_0.26.9-1_amd64.deb &>> $LOG_FILE
		echo "################# Installing package #################" >> $LOG_FILE
		sudo dpkg -i ./klayout_0.26.9-1_amd64.deb &>> $LOG_FILE
		sudo apt-get install -f -y &>> $LOG_FILE
		echo "################# Deleting temp files #################" >> $LOG_FILE
		rm klayout_0.26.9-1_amd64.deb &>> $LOG_FILE
	 	echo "################# Finished #################" >> $LOG_FILE
fi

read -p "Install netgen? [y/n]" -n 1 -r
echo
if [[ $REPLY =~ ^[Yy]$ ]]; then
	echo "Installing netgen..."
		cd $PDK_TOOLS_FOLDER
		LOG_FILE="$PDK_TOOLS_FOLDER/install-logs/netgen_$TIMESTAMP.log"
		touch $LOG_FILE
		echo "################# Cloning main repo #################" >> $LOG_FILE
		git clone git://opencircuitdesign.com/netgen &>> $LOG_FILE
		cd netgen &>> $LOG_FILE
		echo "################# Checkout of version 1.5 #################" >> $LOG_FILE
		git checkout netgen-1.5 &>> $LOG_FILE
		echo "################# Configuring netgen #################" >> $LOG_FILE
		./configure &>> $LOG_FILE
		echo "################# Running make #################" >> $LOG_FILE
		make &>> $LOG_FILE
		sudo make install &>> $LOG_FILE
		echo "################# Finished #################" >> $LOG_FILE
fi

read -p "Install Open PDKs? [y/n]" -n 1 -r
echo
if [[ $REPLY =~ ^[Yy]$ ]]; then
	echo "Installing Open PDKs..."
		cd $PDK_TOOLS_FOLDER
		LOG_FILE="$PDK_TOOLS_FOLDER/install-logs/open_pdks_$TIMESTAMP.log"
		touch $LOG_FILE
		echo "################# Cloning main repo #################" >> $LOG_FILE
		git clone git://opencircuitdesign.com/open_pdks &>> $LOG_FILE
		cd open_pdks &>> $LOG_FILE
		echo "################# Checkout of version 1.0 #################" >> $LOG_FILE
		git checkout open_pdks-1.0 &>> $LOG_FILE
		echo "################# Configuring sky130A #################" >> $LOG_FILE
		mkdir -p $PDK_TOOLS_FOLDER/sky130A &>> $LOG_FILE
		./configure --enable-sky130-pdk=$PDK_TOOLS_FOLDER/skywater-pdk --with-sky130-local-path=$PDK_TOOLS_FOLDER \
		            --with-ef-style &>> $LOG_FILE
		cd sky130 &>> $LOG_FILE
		echo "################# Running make #################" >> $LOG_FILE
		make &>> $LOG_FILE
		sudo make install &>> $LOG_FILE
		echo "################# Changing sky130A permissions #################" >> $LOG_FILE
		sudo chown ${USER}:${USER} -R ~/skywater/sky130A
		echo "################# Finished #################" >> $LOG_FILE
fi

read -p "Install Open MPW Precheck? [y/n]" -n 1 -r
echo
if [[ $REPLY =~ ^[Yy]$ ]]; then
	echo "Installing Open MPW Precheck..."
		cd $PDK_TOOLS_FOLDER
		LOG_FILE="$PDK_TOOLS_FOLDER/install-logs/open_mpw_precheck_$TIMESTAMP.log"
		touch $LOG_FILE
		echo "################# Cloning main repo #################" >> $LOG_FILE
		git clone https://github.com/efabless/open_mpw_precheck.git &>> $LOG_FILE
		echo "################# Get docker image #################" >> $LOG_FILE
		docker pull efabless/open_mpw_precheck:latest &>> $LOG_FILE
		echo "################# Configuring parameters #################" >> $LOG_FILE
		export PDK_PATH="$PDK_TOOLS_FOLDER" &>> $LOG_FILE
		#export TARGET_PATH="path/to/your/project" # This is the path to your project 
		echo "################# Finished #################" >> $LOG_FILE
fi

read -p "Install OpenLane? [y/n]" -n 1 -r
echo
if [[ $REPLY =~ ^[Yy]$ ]]; then
	echo "Installing OpenLane..."
		cd $PDK_TOOLS_FOLDER
		LOG_FILE="$PDK_TOOLS_FOLDER/install-logs/openlane_$TIMESTAMP.log"
		touch $LOG_FILE
		echo "################# Cloning main repo #################" >> $LOG_FILE
		git clone https://github.com/efabless/openlane.git &>> $LOG_FILE
		cd openlane &>> $LOG_FILE
		echo "################# Running make #################" >> $LOG_FILE
		make openlane &>> $LOG_FILE
		echo "################# Configuring parameters #################" >> $LOG_FILE
		export PDK_ROOT="$PDK_TOOLS_FOLDER" &>> $LOG_FILE
		#make pdk &>> $LOG_FILE
		#make test # This is to test that the flow and the pdk were properly installed
		echo "################# Finished #################" >> $LOG_FILE
fi
