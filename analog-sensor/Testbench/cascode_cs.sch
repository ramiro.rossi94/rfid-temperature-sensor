v {xschem version=2.9.9 file_version=1.2 }
G {}
K {}
V {}
S {}
E {}
T {Press ctrl-click left to execute annote} -620 -90 0 0 0.4 0.4 {}
N -20 -60 -20 -50 { lab=v_sm1}
N -20 -50 -20 -40 { lab=v_sm1}
N 40 -90 110 -90 { lab=v_sm2}
N -20 -130 40 -130 { lab=v_sm2}
N 40 -130 40 -90 { lab=v_sm2}
N 40 -10 110 -10 { lab=v_sm1}
N -20 -50 40 -50 { lab=v_sm1}
N 40 -50 40 -10 { lab=v_sm1}
N -20 30 -20 50 { lab=GND}
N 150 30 150 50 { lab=GND}
N 150 -60 150 -40 { lab=#net1}
N -150 -170 -150 -150 { lab=GND}
N 220 -250 220 -230 { lab=GND}
N 150 -310 220 -310 { lab=#net2}
N 150 -10 170 -10 { lab=GND}
N 170 -10 170 30 { lab=GND}
N 150 30 170 30 { lab=GND}
N -40 -10 -20 -10 { lab=GND}
N -40 -10 -40 30 { lab=GND}
N -40 30 -20 30 { lab=GND}
N -20 -130 -20 -120 { lab=v_sm2}
N -150 -320 50 -320 { lab=#net3}
N -150 -320 -150 -230 { lab=#net3}
N -120 -90 -20 -90 { lab=GND}
N 150 -90 250 -90 { lab=GND}
N 150 -140 170 -140 { lab=v_sm5}
N 150 -140 150 -120 { lab=v_sm5}
N -70 -290 -20 -290 { lab=GND}
N 20 -290 50 -290 { lab=#net3}
N 50 -320 50 -290 { lab=#net3}
N -20 -240 -20 -230 { lab=v_sm2}
N -20 -170 -20 -130 { lab=v_sm2}
N 20 -90 40 -90 { lab=v_sm2}
N 20 -10 40 -10 { lab=v_sm1}
N 150 20 150 30 { lab=GND}
N -20 20 -20 30 { lab=GND}
N 150 -250 150 -140 { lab=v_sm5}
N -20 -260 -20 -240 { lab=v_sm2}
N -20 -230 -20 -160 { lab=v_sm2}
C {sky130_fd_pr/nfet_01v8.sym} 0 -10 0 1 {name=M1
L=\{lsource\}
W=\{wsource\}
nf=1 
mult=\{msource\}
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {sky130_fd_pr/nfet_01v8.sym} 130 -10 0 0 {name=M4
L=\{lcopy\}
W=\{wcopy\}
nf=1 
mult=\{mcopy\}
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {sky130_fd_pr/nfet_01v8.sym} 130 -90 0 0 {name=M5
L=\{lcopy\}
W=\{wcopy\}
nf=1 
mult=\{mcopy\}
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {res.sym} 150 -280 0 0 {name=R1
value=10k
footprint=1206
device=resistor
m=1}
C {vsource.sym} 220 -280 0 0 {name=V1 value=\{vdd\}}
C {gnd.sym} 220 -230 0 0 {name=l2 lab=GND}
C {vsource.sym} -150 -200 0 0 {name=V2 value=\{vdd\}}
C {gnd.sym} 220 -230 0 0 {name=l3 lab=GND}
C {gnd.sym} -150 -150 0 0 {name=l4 lab=GND}
C {gnd.sym} 150 50 0 0 {name=l5 lab=GND}
C {gnd.sym} -20 50 0 0 {name=l6 lab=GND}
C {netlist_not_shown.sym} -320 -280 0 0 {name=SIMULATION only_toplevel=false

value="

* Select corner
.include /home/eamta/eamta2021/sch/rfid-temperature-sensor/analog-sensor/Testbench/corner_ff.sp

* Input Parameters
.param vdd = 1.8

* PMOS Resistor
.param wres = 40
.param lres = 1.05
.param mres = 1

* Current source
.param wsource = 19.5
.param lsource = 0.45
.param msource = 1

* Current copy
.param wcopy = \{wsource\}
.param lcopy = \{lsource\}
.param mcopy = 1

*Simulations
.save @M.XM1.msky130_fd_pr__nfet_01v8[id]

.control

   * Operation Point
   op
   save all
   write cascode_cs.raw

   * transicent analysis
   tran 1u 10m 1m 
   
   * Parametric sweep
   *echo '=================== PARAMETRIC SWEEPS ====================='
   *foreach ll 1.5 1.65 1.8 1.95 2.1 2.25 2.4 2.55 2.7
     
    *  alterparam wsource = $ll
     * alterparam wcopy = $ll
      *print $ll
      *let Ierror = (0.1m -((1.8-v_sm5)/10000))/0.1m
      *print Ierror
   *end  
.endc

.end
"}
C {gnd.sym} 250 -90 2 0 {name=l1 lab=GND}
C {gnd.sym} -120 -90 0 0 {name=l7 lab=GND}
C {lab_wire.sym} -7.5 -130 1 0 {name=l8 sig_type=std_logic lab=v_sm2

}
C {lab_wire.sym} 42.5 -10 2 0 {name=l9 sig_type=std_logic lab=v_sm1

}
C {lab_wire.sym} 162.5 -140 2 0 {name=l10 sig_type=std_logic lab=v_sm5

}
C {sky130_fd_pr/nfet_01v8.sym} 0 -90 0 1 {name=M2
L=\{lsource\}
W=\{wsource\}
nf=1 
mult=\{msource\}
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {sky130_fd_pr/nfet_01v8.sym} 0 -290 2 0 {name=M3
L=\{lres\}
W=\{wres\}
nf=1 
mult=\{mres\}
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {gnd.sym} -70 -290 0 0 {name=l12 lab=GND}
C {ngspice_get_value.sym} -20 -130 0 1 {name=r2 node=i(@M.XM2.msky130_fd_pr__nfet_01v8[id])
descr="I= "}
C {launcher.sym} -350 -30 0 0 {name=h1
descr=Annotate 
tclcommand="ngspice::annotate"}
