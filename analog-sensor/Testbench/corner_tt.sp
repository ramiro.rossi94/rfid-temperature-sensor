* PARAMETERS FOR CORNER TT
.param vdd = 1.8
.param iref = 100u
.options TEMP = 65.0

* Fabrication process
.lib ~/skywater/skywater-pdk/libraries/sky130_fd_pr_ngspice/latest/models/corners/sky130.lib TT
