import os

##########################################################################################################################
# Analog Library
pr_lib_dir = os.path.join(os.path.expanduser("~"), "skywater/skywater-pdk/libraries/sky130_fd_pr/latest/")
# High Density Standard Cell Library
sc_hd_lib_dir = os.path.join(os.path.expanduser("~"), "skywater/skywater-pdk/libraries/sky130_fd_sc_hd/latest/cells")
# High Density, Low Leakage Standard Cell Library
sc_hdll_lib_dir = os.path.join(
    os.path.expanduser("~"), "skywater/skywater-pdk/libraries/sky130_fd_sc_hdll/latest/cells"
)
# Low Voltage (<2.0V), Low Speed, Standard Cell Library
sc_ls_lib_dir = os.path.join(os.path.expanduser("~"), "skywater/skywater-pdk/libraries/sky130_fd_sc_ls/latest/cells")
# Low Voltage (<2.0V), Medium Speed, Standard Cell Library
sc_ms_lib_dir = os.path.join(os.path.expanduser("~"), "skywater/skywater-pdk/libraries/sky130_fd_sc_ms/latest/cells")
# Low Voltage (<2.0V), High Speed Standard Cell Library
sc_hs_lib_dir = os.path.join(os.path.expanduser("~"), "skywater/skywater-pdk/libraries/sky130_fd_sc_hs/latest/cells")
# Low Voltage (<2.0V), Low Power, Standard Cell Library
sc_lp_lib_dir = os.path.join(os.path.expanduser("~"), "skywater/skywater-pdk/libraries/sky130_fd_sc_lp/latest/cells")
# High Voltage (5V), Standard Cell Library
sc_hvl_lib_dir = os.path.join(os.path.expanduser("~"), "skywater/skywater-pdk/libraries/sky130_fd_sc_hvl/latest/cells")

##########################################################################################################################
sc_map = {
    "hd": sc_hd_lib_dir,
    "hdll": sc_hdll_lib_dir,
    "ls": sc_ls_lib_dir,
    "ms": sc_ms_lib_dir,
    "hs": sc_hs_lib_dir,
    "lp": sc_lp_lib_dir,
    "hvl": sc_hvl_lib_dir,
}

lib_name = "sky130.lib"
line_decorator = "*************************************\n"

##########################################################################################################################
def make_analog():
    # Read analog lib in lines
    pr_lib_file = os.path.join(pr_lib_dir, "models/sky130.lib.spice")
    try:
        with open(pr_lib_file, "r") as originalLib:
            pr_lib_lines = originalLib.readlines()
    except FileNotFoundError:
        print('Missing pdk directory "{}"'.format(pr_lib_dir))
        return

    # Saving parameters
    with open(lib_name, "a") as file:
        # Saving analog corners
        copying_lib = False
        pr_lib_title = ""
        for pr_lib_line in pr_lib_lines:
            if pr_lib_line.startswith(".lib"):
                copying_lib = True
                file.write(line_decorator + pr_lib_title + line_decorator)
            elif pr_lib_line.startswith("* Resistor"):
                copying_lib = False
            elif pr_lib_line.startswith("* Special"):
                copying_lib = True
            elif pr_lib_line.startswith(".endl"):
                copying_lib = False
                file.write(pr_lib_line + "\n")
                continue

            if copying_lib == False:
                pr_lib_title = pr_lib_line
            else:
                if ".lib" in pr_lib_line:
                    file.write(pr_lib_line)
                elif "/nfet_01v8" in pr_lib_line or "/pfet_01v8" in pr_lib_line:
                    file.write(pr_lib_line.replace("../", pr_lib_dir))
                elif "corners/" in pr_lib_line:
                    file.write(pr_lib_line.replace("corners/", pr_lib_dir + "models/corners/"))
                elif "all.spice" in pr_lib_line:
                    file.write(pr_lib_line.replace("all.spice", pr_lib_dir + "models/all.spice"))
                elif "* " in pr_lib_line:
                    file.write(pr_lib_line)


##########################################################################################################################
def make_digital(sc_lib):
    sc_lib_dir = sc_map.get(sc_lib, "/nonexisting")

    # Get digital spice files
    if not os.path.exists(sc_lib_dir):
        print('Missing pdk directory "{}"'.format(sc_lib_dir))
        return

    sc_logic_files = list()
    sc_buffer_files = list()
    sc_inverter_files = list()
    sc_arithmetic_files = list()
    sc_latch_files = list()
    sc_analog_files = list()
    sc_isolator_files = list()
    sc_uncategorized_files = list()
    for root, dirs, files in os.walk(sc_lib_dir):
        for file_name in files:
            if file_name.endswith(".spice"):
                sc_spice_file_path = os.path.join(root, file_name)
                sc_spice_folder = root.split("/cells")[1]
                if (
                    "/a2" in sc_spice_folder
                    or "/a3" in sc_spice_folder
                    or "/a4" in sc_spice_folder
                    or "/and" in sc_spice_folder
                    or "/clkmux" in sc_spice_folder
                    or "/maj" in sc_spice_folder
                    or "/mux" in sc_spice_folder
                    or "/nand" in sc_spice_folder
                    or "/nor" in sc_spice_folder
                    or "/o2" in sc_spice_folder
                    or "/o3" in sc_spice_folder
                    or "/o4" in sc_spice_folder
                    or "/or" in sc_spice_folder
                    or "/xnor" in sc_spice_folder
                    or "/xor" in sc_spice_folder
                ):
                    sc_logic_files.append(sc_spice_file_path)
                elif (
                    "/buf" in sc_spice_folder
                    or "/clkbuf" in sc_spice_folder
                    or "/clkdlybuf" in sc_spice_folder
                    or "/dlybuf" in sc_spice_folder
                    or "/dlygate" in sc_spice_folder
                    or "/ebuf" in sc_spice_folder
                    or "/lsbufhv" in sc_spice_folder
                    or "/lsbuflv" in sc_spice_folder
                    or "/lsbuf/" in sc_spice_folder
                    or "/schmittbuf" in sc_spice_folder
                ):
                    sc_buffer_files.append(sc_spice_file_path)
                elif (
                    "/clkdlyinv" in sc_spice_folder
                    or "/clkinv" in sc_spice_folder
                    or "/dlymetal" in sc_spice_folder
                    or "/einv" in sc_spice_folder
                    or "/inv" in sc_spice_folder
                ):
                    sc_inverter_files.append(sc_spice_file_path)
                elif "/fa" in sc_spice_folder or "/ha" in sc_spice_folder:
                    sc_arithmetic_files.append(sc_spice_file_path)
                elif (
                    "/dfb" in sc_spice_folder
                    or "/dfr" in sc_spice_folder
                    or "/dfs" in sc_spice_folder
                    or "/dfx" in sc_spice_folder
                    or "/dlc" in sc_spice_folder
                    or "/dlr" in sc_spice_folder
                    or "/dlx" in sc_spice_folder
                    or "/edf" in sc_spice_folder
                    or "/sdf" in sc_spice_folder
                    or "/sdl" in sc_spice_folder
                    or "/sed" in sc_spice_folder
                    or "/latchupcell" in sc_spice_folder
                ):
                    sc_latch_files.append(sc_spice_file_path)
                elif (
                    "/decap" in sc_spice_folder
                    or "/diode" in sc_spice_folder
                    or "/conb" in sc_spice_folder
                    or "/probe" in sc_spice_folder
                    or "/macro" in sc_spice_folder
                    or "/tap" in sc_spice_folder
                    or "/fill" in sc_spice_folder
                ):
                    sc_analog_files.append(sc_spice_file_path)
                elif (
                    "/lpflow" in sc_spice_folder
                    or "/inputiso" in sc_spice_folder
                    or "/iso" in sc_spice_folder
                    or "/lsbufiso" in sc_spice_folder
                ):
                    sc_isolator_files.append(sc_spice_file_path)
                else:
                    sc_uncategorized_files.append(sc_spice_file_path)

    # Saving parameters
    with open(lib_name, "a") as file:

        # Saving stadard digital logic cells
        file.write(
            line_decorator
            + "* "
            + sc_lib
            + " Standard logic cells ("
            + sc_lib
            + "sclog)\n"
            + line_decorator
            + ".lib "
            + sc_lib
            + "sclog\n"
        )
        for sc_spice_path in sorted(sc_logic_files):
            file.write('.include "{}"\n'.format(sc_spice_path))
        file.write(".endl\n\n")

        # Saving stadard digital buffer cells
        file.write(
            line_decorator
            + "* "
            + sc_lib
            + " Standard logic buffers ("
            + sc_lib
            + "scbuf)\n"
            + line_decorator
            + ".lib "
            + sc_lib
            + "scbuf\n"
        )
        for sc_spice_path in sorted(sc_buffer_files):
            file.write('.include "{}"\n'.format(sc_spice_path))
        file.write(".endl\n\n")

        # Saving stadard digital inverter cells
        file.write(
            line_decorator
            + "* "
            + sc_lib
            + " Standard logic inverters ("
            + sc_lib
            + "scinv)\n"
            + line_decorator
            + ".lib "
            + sc_lib
            + "scinv\n"
        )
        for sc_spice_path in sorted(sc_inverter_files):
            file.write('.include "{}"\n'.format(sc_spice_path))
        file.write(".endl\n\n")

        # Saving stadard digital arithmetic cells
        file.write(
            line_decorator
            + "* "
            + sc_lib
            + " Standard arithmetic cells ("
            + sc_lib
            + "scari)\n"
            + line_decorator
            + ".lib "
            + sc_lib
            + "scari\n"
        )
        for sc_spice_path in sorted(sc_arithmetic_files):
            file.write('.include "{}"\n'.format(sc_spice_path))
        file.write(".endl\n\n")

        # Saving stadard digital latch/ff cells
        file.write(
            line_decorator
            + "* "
            + sc_lib
            + " Standard latch/FF cells ("
            + sc_lib
            + "sclth)\n"
            + line_decorator
            + ".lib "
            + sc_lib
            + "sclth\n"
        )
        for sc_spice_path in sorted(sc_latch_files):
            file.write('.include "{}"\n'.format(sc_spice_path))
        file.write(".endl\n\n")

        # Saving stadard digital analog cells
        file.write(
            line_decorator
            + "* "
            + sc_lib
            + " Standard analog cells ("
            + sc_lib
            + "scanl)\n"
            + line_decorator
            + ".lib "
            + sc_lib
            + "scanl\n"
        )
        for sc_spice_path in sorted(sc_analog_files):
            file.write('.include "{}"\n'.format(sc_spice_path))
        file.write(".endl\n\n")

        # Saving stadard digital isolator cells
        file.write(
            line_decorator
            + "* "
            + sc_lib
            + " Standard isolated cells ("
            + sc_lib
            + "sciso)\n"
            + line_decorator
            + ".lib "
            + sc_lib
            + "sciso\n"
        )
        for sc_spice_path in sorted(sc_isolator_files):
            file.write('.include "{}"\n'.format(sc_spice_path))
        file.write(".endl\n\n")

        # Saving stadard digital uncategorized cells
        if sc_uncategorized_files:
            file.write(
                line_decorator
                + "* "
                + sc_lib
                + " Standard uncategorized cells ("
                + sc_lib
                + "scunk)\n"
                + line_decorator
                + ".lib "
                + sc_lib
                + "scunk\n"
            )
            for sc_spice_path in sorted(sc_uncategorized_files):
                file.write('.include "{}"\n'.format(sc_spice_path))
            file.write(".endl\n\n")


##########################################################################################################################
def main():
    # Clear file
    with open(lib_name, "w+") as file:
        file.close()

    make_analog()
    for key in sc_map:
        make_digital(key)


if __name__ == "__main__":
    main()
