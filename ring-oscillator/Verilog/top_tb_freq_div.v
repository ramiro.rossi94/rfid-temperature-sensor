//! @title  Ring oscillator temperature sensor - top ring oscillator file testbench
//! @file   top_tb.v
//! @author Ramiro Javier Rossi - UTN Researcher
//! @date   29-10-2021

`include "frequency_divider.v"
`timescale 1ns/1ps

module top_tb();

    parameter DIVIDER = 128;

    localparam reset = 0;

    reg  i_signal = 1'b0 ;
    reg  i_enable = 1'b0 ;

    wire o_signal;

    freq_divider
        u_freq_divider
        (
            .i_frequency(i_signal),
            .i_enable   (i_enable),
            .o_divided_requency (o_signal)
        );

    //Clock generation
    always #5 i_signal = ~i_signal;

    real i;
    initial begin
        $display("");
        $display("Simulation Started");
        $dumpfile("top_tb_freq_div.vcd");
        $dumpvars(0, top_tb);

        //Reset variables
        i_enable = reset;
        i_signal = reset;

        //Enable ring oscillator
        #10 i_enable = 1'b1;

        #1000 i_enable = 1'b0;

        $display("Simulation Finished");
        $display("");
        $finish;
    end

endmodule //top_tb
