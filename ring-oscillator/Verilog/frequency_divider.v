//! @title  frequency divider
//! @file   frequency_divider.v
//! @author Ramiro Javier Rossi - UTN Researcher
//! @date   21-10-2021

//! - Frequency Divider
//! - **i_frequency** is the frequency divider input frequency.
//! - **o_divided_requency** is the frequency divider output frequency (already divided).

module freq_divider 
    #(
        parameter DIVIDER = 8      //! Coefficient that will divide the input freequency
    ) 
    (
        input   i_frequency,       //! Input frequency. Must connect to Ring oscillator output
        input   i_enable   ,       //! Enable input
        output  o_divided_requency //! Frequency output after dividing it
    );
    
    //! Internal Parameters
    localparam output_reset = 1'b0;

    //! Internal Vars
    reg [DIVIDER:0] count = 0;     
    reg             output_clock_reg = 1'b0;  
    reg             i_enable_reg = 1'b0;

    assign o_divided_requency = ( i_enable_reg ) ? output_clock_reg : output_reset;

    always @(posedge i_frequency ) begin
        if ( count < DIVIDER ) 
            count <= count + 1;
        else begin
           output_clock_reg <= ~ output_clock_reg;
           count <= 0;
        end
    end

    always @(posedge i_enable ) begin
        i_enable_reg <= ~i_enable_reg;
    end

endmodule //freq_divider
