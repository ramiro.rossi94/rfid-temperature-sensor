In order to use the OpenLane tool to get the GDS files from the verilog its necesary to:
1. Install OpenLane as its described in https://github.com/The-OpenROAD-Project/OpenLane
2. Start the OpenLane docker with the "make mount" command inside the OpenLane source directory
3. Copy and paste the Verilog sources files inside the OpenLane source directory under a folder name "my_designs"
4. Create a new design with the following command:
 /flow.tcl -design ring_oscillator_sensor -src "list all the .v one by one (remember to add my_desing/" -init_design_config. For example:
 ./flow.tcl -design ring_oscillator_sensor -src "my_design/blinky.v my_design/counter.v my_design/frequency_divider.v my_design/inverter.v my_design/ring_osc.v my_design/ring_oscillator_sensor.v" -init_design_config
 NOTE: After the -design put the name of your design, in this case its called "ring_oscillator_sensor"
5. Change the name of the inverter module (inside inverter.v) to "inv" (this is done due to that already exists another inverter module inside the OpenLane's designs folder)
6. After folloed all the previos steps simply run:
 ./flow.tcl -design ring_oscillator_sensor
 NOTE: As said above after design its followed the name you put to your design
7. Everything is done!
To see the new design using magic:
1. Install magic downloading the .tar directly from magic source page
2. Run the command: 
 magic -rf magicrc
 NOTE: From where the magicrc script is located (it can be found under the eampta-opamp repo)
3. Simply open the .gds file create after run OpenLane with your design using magic. It can be found under 
$(DESIGN_DIR)/src/runs/THE_NAME_YOU_PUT_TO_THE_RUN/results/finishing
