//! @title  Ring oscillator temperature sensor - top ring oscillator file testbench
//! @file   top_tb.v
//! @author Ramiro Javier Rossi - UTN Researcher
//! @date   29-10-2021

`include "counter.v"
`timescale 1ns/1ps

module top_tb();

    parameter NB_INT_COUNTER = 7;

    localparam reset = 0;

    reg  i_signal = 1'b0 ;
    reg  i_enable;
    reg  tb_clk = 1'b1;

    wire [NB_INT_COUNTER:0] o_signal;

    counter
        u_counter
        (
            .i_signal(i_signal),
            .i_enable(i_enable),
            .clock   (tb_clk),
            .o_count (o_signal)
        );

    // Clock generation
    always #5 tb_clk = ~tb_clk;

    real i;
    initial begin
        $display("");
        $display("Simulation Started");
        $dumpfile("top_tb_counter.vcd");
        $dumpvars(0, top_tb);

        //Reset variables
        i_enable = reset;

        //Enable ring oscillator
        #10 i_enable = 1'b1;

        for ( i=0 ; i < 2**NB_INT_COUNTER ; i=i+1 ) begin
            i_signal <= ~i_signal;
            #100;
        end

        #1000 i_enable = 1'b0;

        $display("Simulation Finished");
        $display("");
        $finish;
    end

endmodule //top_tb
