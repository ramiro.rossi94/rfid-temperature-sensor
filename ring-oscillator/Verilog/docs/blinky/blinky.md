# Blinnky module for debug purpose

- **File**: blinky.v
- **Author:** Ramiro Javier Rossi - UTN Researcher
- **Date:** 7-11-2021
- **Brief:**  Makes a led blink if there is any output after the frequency_divider block 
## Diagram

![Diagram](blinky.svg "Diagram")
## Description


 



 - Blinnky
 - **i_enable** is the counter's enable input.
 - **clock**    is the counter's clock input.
 - **o_count**  is the counter's output with the corresponds count.

## Generics

| Generic name | Type | Value | Description |
| ------------ | ---- | ----- | ----------- |
| NB_COUNTER   |      | 31    |             |
## Ports

| Port name | Direction | Type | Description                                                                       |
| --------- | --------- | ---- | --------------------------------------------------------------------------------- |
| i_enable  | input     |      | Module enable input                                                               |
| clock     | input     |      | Clock input                                                                       |
| o_to_led  | output    |      | Led output. Turn on after hitting enable switch. Turn it off hitting enable again |
## Signals

| Name           | Type                 | Description   |
| -------------- | -------------------- | ------------- |
| counter        | reg [NB_COUNTER : 0] | Internal Vars |
| led_status_reg | reg                  |               |
| i_enable_reg   | reg                  |               |
## Constants

| Name        | Type | Value       | Description         |
| ----------- | ---- | ----------- | ------------------- |
| CLOCK_FREQ  |      | 12000000    | Internal Parameters |
| T_PERIOD_MS |      | 500         |                     |
| COUNT_MAX   |      | T_PERIOD_MS |                     |
| LED_ENABLE  |      | 1'b1        |                     |
| LED_DISABLE |      | 1'b0        |                     |
## Processes
- unnamed: ( @(posedge clock ) )
  - **Type:** always
- unnamed: ( @(negedge clock ) )
  - **Type:** always
- unnamed: ( @(posedge i_enable ) )
  - **Type:** always
