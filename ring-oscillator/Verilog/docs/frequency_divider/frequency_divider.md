# frequency divider

- **File**: frequency_divider.v
- **Author:** Ramiro Javier Rossi - UTN Researcher
- **Date:** 21-10-2021
- **Brief:** Divides the input of the input signal frequency according to the value of the parameter DIVIDER
## Diagram

![Diagram](frequency_divider.svg "Diagram")
## Description


 



 - Frequency Divider
 - **i_frequency** is the frequency divider input frequency.
 - **o_divided_requency** is the frequency divider output frequency (already divided).

## Generics

| Generic name | Type | Value | Description                                       |
| ------------ | ---- | ----- | ------------------------------------------------- |
| DIVIDER      |      | 8     | Coefficient that will divide the input freequency |
## Ports

| Port name          | Direction | Type | Description                                             |
| ------------------ | --------- | ---- | ------------------------------------------------------- |
| i_frequency        | input     |      | Input frequency. Must connect to Ring oscillator output |
| i_enable           | input     |      | Enable input                                            |
| o_divided_requency | output    |      | Frequency output after dividing it                      |
## Signals

| Name             | Type            | Description   |
| ---------------- | --------------- | ------------- |
| count            | reg [DIVIDER:0] | Internal Vars |
| output_clock_reg | reg             |               |
| i_enable_reg     | reg             |               |
## Constants

| Name         | Type | Value | Description         |
| ------------ | ---- | ----- | ------------------- |
| output_reset |      | 1'b0  | Internal Parameters |
## Processes
- unnamed: ( @(posedge i_frequency ) )
  - **Type:** always
- unnamed: ( @(posedge i_enable ) )
  - **Type:** always
