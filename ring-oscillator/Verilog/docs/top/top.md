# Ring oscillator temperature sensor - top file

- **File**: top.v
- **File:** top.v
- **Author:** Ramiro Javier Rossi - UTN Researcher
- **Date:** 29-10-2021
## Diagram

![Diagram](top.svg "Diagram")
## Description



 


 - Top
 - **i_enable** is the block enable input.
 - **o_led__module_enable** is the block output to the led. Turn on each time you press the enable button.
 - **o_freq_divider** is the block output to the specified pin in the board. It has the value of the ring oscillator frequency divided by the DIVIDER parameter

## Generics

| Generic name | Type | Value | Description                                     |
| ------------ | ---- | ----- | ----------------------------------------------- |
| DIV          |      | 12    | Divider of the ring-oscillator output frequency |
| NUM_INVERTER |      | 12    | Number of inverters inside the ring-oscillator  |
## Ports

| Port name            | Direction | Type | Description                 |
| -------------------- | --------- | ---- | --------------------------- |
| i_enable             | input     |      | enable signal               |
| clock                | input     |      | Clock signal                |
| o_led__module_enable | output    |      | LED indicating module is ON |
| o_freq_divider       | output    |      | Freq divider output         |
## Signals

| Name         | Type     | Description                                    |
| ------------ | -------- | ---------------------------------------------- |
| NUM_INVERTER | arameter | Number of inverters inside the ring-oscillator |
## Instantiations

- u_ring_osc: ring_osc
- u_freq_divider: freq_divider
- u_blinky: blinky
