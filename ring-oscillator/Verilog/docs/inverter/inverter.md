# inverter

- **File**: inverter.v
- **Author:** Ramiro Javier Rossi - UTN Researcher
- **Date:** 19-10-2021
- **Brief:** Negates input
## Diagram

![Diagram](inverter.svg "Diagram")
## Description


 



 - Inverter
 - **i_data** is the inverter input.
 - **o_data** inverter output

## Ports

| Port name | Direction | Type | Description     |
| --------- | --------- | ---- | --------------- |
| i_data    | input     |      | inverter input  |
| o_data    | output    |      | inverter output |
## Signals

| Name   | Type | Description |
| ------ | ---- | ----------- |
| auxReg | reg  |             |
## Processes
- unnamed: ( @(*) )
  - **Type:** always
