# Ring oscillator

- **File**: ring_osc.v
- **File:** ring_osc.v
- **Author:** Ramiro Javier Rossi - UTN Researcher
- **Date:** 19-10-2021
## Diagram

![Diagram](ring_osc.svg "Diagram")
## Description



 


 - Ring oscillator (previously delayline)
 - **i_ring** is the ring oscillator input.
 - **o_ring** ring oscillator output

## Generics

| Generic name     | Type | Value | Description                                         |
| ---------------- | ---- | ----- | --------------------------------------------------- |
| NUMBER_INVERTERS |      | 30    | Number of inverter that compose the ring oscillator |
## Ports

| Port name | Direction | Type | Description            |
| --------- | --------- | ---- | ---------------------- |
| i_enable  | input     |      | ring oscillator enable |
| o_ring    | output    |      | ring oscillator output |
## Signals

| Name            | Type | Description |
| --------------- | ---- | ----------- |
| i_enable_reg    | reg  |             |
| inverter_input  | wire |             |
| inverter_output | wire |             |
## Constants

| Name         | Type | Value | Description |
| ------------ | ---- | ----- | ----------- |
| output_reset |      | 0     |             |
## Processes
- unnamed: ( @(posedge i_enable ) )
  - **Type:** always
## Instantiations

- u_inverter: inverter
