//! @title  counter
//! @file   counter.v
//! @author Ramiro Javier Rossi - UTN Researcher
//! @date   20-10-2021

//! - Counter
//! - **i_signal** is the counter' signal input.
//! - **i_enable** is the counter's enable input.
//! - **clock**    is the counter's clock input.
//! - **o_count**  is the counter's output with the corresponds count.

module counter 
    #(
        parameter NB_INT_COUNTER = 7  //! Counter bits quantity
    )
    (
        input   i_signal,   //! Input signal
        input   i_enable,   //! Module enable input
        input   clock,      //! Clock input
        output  o_error,    //! Error output. Enables if counter if greater than 2**NB_INT_COUNTER-1
        output  [NB_INT_COUNTER:0] o_count  //! Counter output
    );

    //! Internal Parameters
    localparam internal_counter_reset = 8'h00;
    localparam counter_limit  = 2**NB_INT_COUNTER - 1;
    localparam NO_ERROR_OCCURRED = 1'b0 ;

    //! Internal Vars
    reg [NB_INT_COUNTER:0]  internal_counter_reg = internal_counter_reset;
    reg                     counter_enable       = 1'b0;
    reg                     error_occurred       = 1'b0;
    reg                     i_enable_reg         = 1'b0;

    //Output continuos assingments statements
    assign o_count  = ( counter_enable == 1'b1) ? internal_counter : internal_counter_reset  ;
    assign o_error  = ( error_occurred == 1'b1) ? error_occurred : NO_ERROR_OCCURRED;

    always @(posedge i_signal ) begin
        if ( counter_enable == 1'b1) begin
            if (internal_counter_reg <= counter_limit ) 
                internal_counter_reg <= internal_counter_reg + 1;
            else
                internal_counter_reg <= counter_limit;
                error_occurred <= 1'b1;
        end
        else 
            internal_counter_reg <= internal_counter_reset;
    end

    always @(posedge clock ) begin
        if (i_enable_reg)
            counter_enable <= 1'b1;
        else
            counter_enable <= 1'b0;
    end

    always @(posedge i_enable ) begin
        i_enable_reg <= ~i_enable_reg;
    end

endmodule //counter