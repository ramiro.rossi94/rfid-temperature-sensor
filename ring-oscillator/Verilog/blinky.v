//! @title  Blinnky module for debug purpose
//! @file   blinky.v
//! @author Ramiro Javier Rossi - UTN Researcher
//! @date   7-11-2021

//! - Blinnky
//! - **i_enable** is the counter's enable input.
//! - **clock**    is the counter's clock input.
//! - **o_count**  is the counter's output with the corresponds count.

module blinky #(
    parameter NB_COUNTER = 31
) (
    input i_enable, //! Module enable input
    input clock,    //! Clock input
    output o_to_led //! Led output. Turn on after hitting enable switch. Turn it off hitting enable again
);
    
    //! Internal Parameters
    localparam CLOCK_FREQ  = 12000000;
    localparam T_PERIOD_MS = 500;
    localparam COUNT_MAX   = (CLOCK_FREQ/1000) * T_PERIOD_MS;
    localparam LED_ENABLE  = 1'b1;
    localparam LED_DISABLE = 1'b0;

    //! Internal Vars
    reg [NB_COUNTER : 0] counter = 0;
    reg led_status_reg = 0;
    reg i_enable_reg   = 0;

    always @(posedge clock ) begin
        if( i_enable_reg && counter < COUNT_MAX) begin
          counter <= counter + 1;
        end
        else 
            counter <= 0;
    end

    always @(negedge clock ) begin
        if(i_enable_reg && counter < COUNT_MAX/2)
            led_status_reg <= 1'b1;
        else
            led_status_reg <= 1'b0;
    end

    always @(posedge i_enable ) begin
        i_enable_reg <= ~i_enable_reg;
    end

    //! Continuos assignment
    assign o_to_led = ( led_status_reg ) ? LED_ENABLE : LED_DISABLE;

endmodule