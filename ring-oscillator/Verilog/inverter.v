//! @title  inverter
//! @file   inverter.v
//! @author Ramiro Javier Rossi - UTN Researcher
//! @date   19-10-2021

//! - Inverter
//! - **i_data** is the inverter input.
//! - **o_data** inverter output

module inverter
    (
        input   i_data,      //! inverter input
        output  o_data       //! inverter output
    )/* synthesis syn_hier = "hard" */;

    reg     auxReg = 0;

    always @(*) begin
        auxReg <= ~i_data;
    end

    assign  o_data = auxReg;

endmodule
