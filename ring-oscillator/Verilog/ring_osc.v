//! @title  Ring oscillator
//! @file   ring_osc.v
//! @author Ramiro Javier Rossi - UTN Researcher
//! @date   19-10-2021

//! - Ring oscillator (previously delayline)
//! - **i_ring** is the ring oscillator input.
//! - **o_ring** ring oscillator output

`include "inverter.v"

module ring_osc
    #(
        parameter NUMBER_INVERTERS  = 30  //! Number of inverter that compose the ring oscillator
    )
    (
        input   i_enable,//! ring oscillator enable
        output  o_ring   //! ring oscillator output
    );

    localparam output_reset     = 0;

    reg i_enable_reg = 0;

    wire inverter_input     [NUMBER_INVERTERS:0] ;
    wire inverter_output    [NUMBER_INVERTERS:0] ;

    assign inverter_input[0] = i_enable && o_ring;

    //generates 30 inverters automatically
    generate
        genvar i;
        for (i=0 ; i < NUMBER_INVERTERS - 1 ; i=i+1) begin
            inverter
                u_inverter
                    (
                        .i_data (inverter_input[i]  ),
                        .o_data (inverter_output[i] )
                    );
            assign inverter_output[i] = inverter_input[i+1];
         end
    endgenerate

    //Creating last inverter, its output its the ring oscillator output
    inverter
        u_inverter
            (
                .i_data (inverter_input[NUMBER_INVERTERS - 1]  ),
                .o_data (inverter_output[NUMBER_INVERTERS - 1] )
            );

    always @(posedge i_enable ) begin
        i_enable_reg <= ~i_enable_reg;
    end

    //assign o_ring = inverter_output[NUMBER_INVERTERS - 1];
    assign o_ring = (i_enable_reg  == 1'b1) ? inverter_output[NUMBER_INVERTERS - 1] : output_reset;

endmodule //ring_osc
