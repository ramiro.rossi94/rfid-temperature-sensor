//! @title  Ring oscillator temperature sensor - top ring oscillator file testbench
//! @file   top_tb.v
//! @author Ramiro Javier Rossi - UTN Researcher
//! @date   29-10-2021

`include "ring_osc.v"
`timescale 1ns/1ps

module top_tb();

    localparam reset = 0;

    wire o_signal;
    reg i_enable;

    ring_osc
        u_ring_osc
        (
            .i_enable(i_enable),
            .o_ring(o_signal)
        );

    initial begin
        $display("");
        $display("Simulation Started");
        $dumpfile("top_tb_ring_osc.vcd");
        $dumpvars(0, top_tb);

        //Reset variables
        i_enable = reset;

        //Enable ring oscillator
        #10 i_enable = 1'b1;

        #1000 i_enable = 1'b0;

        $display("Simulation Finished");
        $display("");
        $finish;
    end

endmodule //top_tb
