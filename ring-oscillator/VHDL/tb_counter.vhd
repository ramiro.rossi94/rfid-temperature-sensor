--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   17:35:30 07/12/2021
-- Design Name:   
-- Module Name:   D:/VHDL/Ring-oscillator-v4.0/tb_counter.vhd
-- Project Name:  Ring-oscillator
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: counter
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY tb_counter IS
END tb_counter;
 
ARCHITECTURE behavior OF tb_counter IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT counter
    PORT(
         SGNL : IN  std_logic;
         CLK : IN  std_logic;
         EN : IN  std_logic;
         COUNT : OUT  std_logic_vector(7 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal SGNL : std_logic := '0';
   signal CLK : std_logic := '0';
   signal EN : std_logic := '0';

 	--Outputs
   signal COUNT : std_logic_vector(7 downto 0);

   -- Clock period definitions
   constant RFID_period : time := 73.7463126843658 ns; -- 13.56 MHz
	constant RING_period : time := 1.88284 ns; -- 531,11 MHz
	--constant RFID_period : time := 21.1 ns; -- 13.56 MHz
	--constant RING_period : time := 1 ns; -- 531,11 MHz
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: counter PORT MAP (
          SGNL => SGNL,
          CLK => CLK,
          EN => EN,
          COUNT => COUNT
        );

   -- Clock process definitions
   CLK_process :process
   begin
		CLK <= '0';
		wait for RFID_period/2;
		CLK <= '1';
		wait for RFID_period/2;
   end process;
	
	SGNL_process :process
   begin
		SGNL <= '0';
		wait for RING_period/2;
		SGNL <= EN;
		wait for RING_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      wait for 100 ns;	

      wait for RFID_period*10;

      -- insert stimulus here 
		
		EN <= '1';
		wait for RFID_period*10;
		EN <= '0';
		wait for RFID_period*3;
		EN <= '1';
		wait for RFID_period*2;
		EN <= '0';
		wait for RFID_period*2;
      --wait;
   end process;

END;
