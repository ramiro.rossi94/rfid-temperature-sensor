----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
----------------------------------------------------------------------------------
entity frecuency_divider is
port (
	freq_in: 	in STD_LOGIC;
	freq_div:		out STD_LOGIC
);
end frecuency_divider;
----------------------------------------------------------------------------------
architecture rtl of frecuency_divider is
	constant max_count: INTEGER := 128;
	signal count: INTEGER range 0 to max_count;
	signal clk_state: STD_LOGIC := '0';
	
begin
	gen_clock: process(freq_in, clk_state, count)
	begin
		if freq_in'event and freq_in='1' then
			if count < max_count then 
				count <= count+1;
			else
				clk_state <= not clk_state;
				count <= 0;
			end if;
		end if;
	end process;
	
	persecond: process (clk_state)
	begin
		freq_div <= clk_state;
	end process;
	
end rtl;

