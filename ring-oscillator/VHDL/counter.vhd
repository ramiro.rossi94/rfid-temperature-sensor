----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    20:50:23 11/24/2020 
-- Design Name: 
-- Module Name:    counter - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;


entity counter is
    Port ( SGNL : in STD_LOGIC;
			  CLK : in STD_LOGIC;
			  EN : in STD_LOGIC;
           COUNT : out  STD_LOGIC_VECTOR(7 downto 0)
			  );
end counter;


architecture rtl of counter is
   signal INTERNAL_COUNTER : STD_LOGIC_VECTOR (7 downto 0) := "00000000";
	signal GATE : STD_LOGIC := '0';
	begin
	
	gate_clk: process (CLK, EN) begin
		if (rising_edge(CLK)) then
			GATE <= not(GATE) and EN;
		end if;
	end process gate_clk;
	
	count_clk: process (SGNL, GATE) begin
		if (rising_edge(SGNL) and GATE = '1') then
			INTERNAL_COUNTER <= INTERNAL_COUNTER + 1;
		elsif (GATE = '0') then
			INTERNAL_COUNTER <= "00000000";
		end if;
	end process count_clk;
	
	update_output: process (GATE, EN) begin
		if (falling_edge(GATE)) then
			COUNT <= INTERNAL_COUNTER;
		elsif (EN = '0') then
			COUNT <= "00000000";
		end if;
		
	end process update_output;
	
end architecture;