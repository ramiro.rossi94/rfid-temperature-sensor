----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
----------------------------------------------------------------------------------
entity delayline is
    Port ( sgnl 			: in  STD_LOGIC;
           delayed_sgnl : out  STD_LOGIC);
end delayline;
----------------------------------------------------------------------------------
architecture Behavioral of delayline is

	component inverter
		port (a 		: in STD_LOGIC;
				a_not : out STD_LOGIC );
	end component;

	signal avector : STD_LOGIC_VECTOR (0 to 31); -- 31 inverters
	
	attribute keep : string;
	attribute keep of avector: signal is "true";
	
	attribute S: string;
	attribute S of avector : signal is "TRUE";

	begin
		avector(0) <= sgnl; -- signal goes through first inverter
		g1: for i in 0 to 30 generate
			invrt: inverter port map ( avector(i), avector(i+1) ); -- generating chain
		end generate g1;
		delayed_sgnl <= avector(31); --signal comes out last inverter
end Behavioral;

