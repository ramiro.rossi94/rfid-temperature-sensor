* PARAMETERS FOR CORNER TT
.param vdd = 1.8
.param iref = 100u
.options TEMP = 65.0

* Fabrication process
.lib ~/Projects/rfid-temperature-sensor/sky130.lib TT
