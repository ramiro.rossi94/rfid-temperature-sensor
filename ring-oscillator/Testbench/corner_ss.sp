* PARAMETERS FOR CORNER SS
.param vdd = 1.62
.param iref = 95u
.options TEMP = 125.0

* Fabrication process
.lib ~/Projects/rfid-temperature-sensor/sky130.lib SS
