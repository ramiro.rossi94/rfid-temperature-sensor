v {xschem version=2.9.9 file_version=1.2 }
G {}
K {}
V {}
S {}
E {}
N 30 130 30 160 { lab=GND}
N 30 40 30 70 { lab=vdd}
N -20 -70 -20 -40 { lab=vss}
N -20 -160 -20 -130 { lab=in}
N 290 60 320 60 { lab=#net1}
N 960 160 960 190 { lab=GND}
N 170 60 200 60 { lab=in}
N 240 90 240 120 { lab=GND}
N 240 0 240 30 { lab=vdd}
N 200 70 200 110 { lab=GND}
N 200 110 240 110 { lab=GND}
N 440 60 470 60 { lab=#net2}
N 320 60 350 60 { lab=#net1}
N 390 90 390 120 { lab=GND}
N 390 0 390 30 { lab=vdd}
N 350 70 350 110 { lab=GND}
N 350 110 390 110 { lab=GND}
N 590 60 620 60 { lab=#net3}
N 470 60 500 60 { lab=#net2}
N 540 90 540 120 { lab=GND}
N 540 0 540 30 { lab=vdd}
N 500 70 500 110 { lab=GND}
N 500 110 540 110 { lab=GND}
N 740 60 770 60 { lab=#net4}
N 770 60 800 60 { lab=#net4}
N 840 90 840 120 { lab=GND}
N 840 0 840 30 { lab=vdd}
N 800 70 800 110 { lab=GND}
N 800 110 840 110 { lab=GND}
N 890 60 960 60 { lab=out}
N 960 60 960 100 { lab=out}
N 740 60 770 60 { lab=#net4}
N 620 60 650 60 { lab=#net3}
N 690 90 690 120 { lab=GND}
N 690 0 690 30 { lab=vdd}
N 650 70 650 110 { lab=GND}
N 650 110 690 110 { lab=GND}
C {../Design/not_a.sym} 240 60 0 0 {name=x1}
C {vsource.sym} 30 100 0 0 {name=V2 value=DC\{Vdd\}}
C {lab_pin.sym} 30 40 1 0 {name=l9 sig_type=std_logic lab=vdd
}
C {netlist_not_shown.sym} 260 -140 0 0 {name=SIMULATION only_toplevel=false 

value="


* Circuit Parameters
.param vin  = 1.8

* Include Models
.include ~/Projects/rfid-temperature-sensor/ring-oscillator/Testbench/corner_tt.sp

* OP Parameters & Singals to save
.save all
+ @M.X1.XM1.msky130_fd_pr__nfet_01v8[id] @M.X1.XM1.msky130_fd_pr__nfet_01v8[vth] @M.X1.XM1.msky130_fd_pr__nfet_01v8[vgs] @M.X1.XM1.msky130_fd_pr__nfet_01v8[vds] @M.X1.XM1.msky130_fd_pr__nfet_01v8[vdsat] @M.X1.XM1.msky130_fd_pr__nfet_01v8[gm] @M.X1.XM1.msky130_fd_pr__nfet_01v8[gds]
+ @M.X1.XM2.msky130_fd_pr__pfet_01v8[id] @M.X1.XM2.msky130_fd_pr__pfet_01v8[vth] @M.X1.XM2.msky130_fd_pr__pfet_01v8[vgs] @M.X1.XM2.msky130_fd_pr__pfet_01v8[vds] @M.X1.XM2.msky130_fd_pr__pfet_01v8[vdsat] @M.X1.XM2.msky130_fd_pr__pfet_01v8[gm] @M.X1.XM2.msky130_fd_pr__pfet_01v8[gds]

*Simulations
.control
  tran 0.1n 0.5u
  setplot tran1
  plot v(out) v(in)
  
  reset
  dc V3 0 1.8 0.01
  setplot dc1
  plot v(out) v(in)
  
  reset
  op
  print all
  
.endc

.end
"}
C {vsource.sym} -20 -100 0 0 {name=V3 value="PULSE(0 \{Vin\} 1ps 1ps 1ps 50ns 100ns) DC\{Vin\}"}
C {lab_pin.sym} -20 -160 1 0 {name=l23 sig_type=std_logic lab=in}
C {capa.sym} 960 130 0 0 {name=C1
m=1
value=1p
footprint=1206
device="ceramic capacitor"}
C {lab_pin.sym} 240 0 1 0 {name=l6 sig_type=std_logic lab=vdd
}
C {lab_pin.sym} 170 60 0 0 {name=l7 sig_type=std_logic lab=in}
C {lab_pin.sym} 960 60 2 0 {name=l8 sig_type=std_logic lab=out}
C {../Design/not_a.sym} 390 60 0 0 {name=x2}
C {lab_pin.sym} 390 0 1 0 {name=l11 sig_type=std_logic lab=vdd
}
C {../Design/not_a.sym} 540 60 0 0 {name=x3}
C {lab_pin.sym} 540 0 1 0 {name=l13 sig_type=std_logic lab=vdd
}
C {../Design/not_a.sym} 840 60 0 0 {name=x5}
C {lab_pin.sym} 840 0 1 0 {name=l17 sig_type=std_logic lab=vdd
}
C {../Design/not_a.sym} 690 60 0 0 {name=x4}
C {lab_pin.sym} 690 0 1 0 {name=l15 sig_type=std_logic lab=vdd
}
C {gnd.sym} 30 160 0 0 {name=l2 lab=GND}
C {gnd.sym} 240 120 0 0 {name=l4 lab=GND}
C {gnd.sym} 390 120 0 0 {name=l5 lab=GND}
C {gnd.sym} 540 120 0 0 {name=l10 lab=GND}
C {gnd.sym} 690 120 0 0 {name=l12 lab=GND}
C {gnd.sym} 840 120 0 0 {name=l14 lab=GND}
C {gnd.sym} 960 190 0 0 {name=l16 lab=GND}
C {gnd.sym} -20 -40 0 0 {name=l1 lab=GND}
