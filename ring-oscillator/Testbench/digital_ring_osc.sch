v {xschem version=2.9.9 file_version=1.2 }
G {}
K {}
V {}
S {}
E {}
N 80 340 80 350 { lab=GND}
N 80 230 80 260 { lab=vdd}
N 300 300 340 300 { lab=#net1}
N 420 300 460 300 { lab=#net2}
N 540 300 580 300 { lab=#net3}
N 660 300 700 300 { lab=#net4}
N 20 230 20 250 { lab=vdd}
N 20 230 80 230 { lab=vdd}
N 20 310 20 340 { lab=GND}
N 20 340 80 340 { lab=GND}
N 80 320 80 340 { lab=GND}
N 190 300 220 300 { lab=inv1}
N 190 250 190 300 { lab=inv1}
N 780 300 820 300 { lab=#net5}
N 190 250 820 250 { lab=inv1}
N 820 250 820 300 {}
C {vsource.sym} 80 290 0 0 {name=V2 value=DC\{VDD\}}
C {gnd.sym} 80 350 0 0 {name=l1 lab=GND}
C {lab_pin.sym} 80 230 1 0 {name=l9 sig_type=std_logic lab=vdd
}
C {netlist_not_shown.sym} 20 50 0 0 {name=SIMULATION only_toplevel=false 

value="

* Circuit Parameters

* Include Models
.include ~/Projects/rfid-temperature-sensor/ring-oscillator/Testbench/corner_tt.sp
.lib ~/Projects/rfid-temperature-sensor/sky130.lib LPSCINV

* OP Parameters & Singals to save
.save all

*Simulations
.control

  *option temp=10
  *tran 0.001n 100n 95n
  *plot v(inv1)
  *meas tran period TRIG v(inv1) VAL=0.9 RISE=1 TARG V(inv1) VAL=0.9 RISE=2

  let minTemp = 0
  let maxTemp = 100

  echo 'inv,temp,period1' > '~/Projects/rfid-temperature-sensor/ring-oscillator/Testbench/digital_ring_osc.csv'
  let cnt=minTemp
  while cnt <= maxTemp
    echo 'TEMP $&cnt'
    reset
    option temp=$&cnt
    tran 0.001n 1000n 999n
    *plot v(inv1)
    meas tran period TRIG v(inv1) VAL=0.9 RISE=1 TARG V(inv1) VAL=0.9 RISE=2
    set line = 'inv1,$&cnt,$&period'
    echo $line >> '~/Projects/rfid-temperature-sensor/ring-oscillator/Testbench/digital_ring_osc.csv'
    let cnt = cnt + 1
    *let cnt=101
  end

.endc

.end
"}
C {lab_pin.sym} 670 250 1 0 {name=l20 sig_type=std_logic lab=inv1}
C {sky130_stdcells/inv_1.sym} 260 300 0 0 {name=x1 VGND=GND VNB=GND VPB=vdd VPWR=vdd prefix=sky130_fd_sc_lp__ }
C {res.sym} 20 280 0 0 {name=R1
value=100k
footprint=1206
device=resistor
m=1}
C {sky130_stdcells/inv_1.sym} 380 300 0 0 {name=x2 VGND=GND VNB=GND VPB=vdd VPWR=vdd prefix=sky130_fd_sc_lp__ }
C {sky130_stdcells/inv_1.sym} 500 300 0 0 {name=x3 VGND=GND VNB=GND VPB=vdd VPWR=vdd prefix=sky130_fd_sc_lp__ }
C {sky130_stdcells/inv_1.sym} 620 300 0 0 {name=x4 VGND=GND VNB=GND VPB=vdd VPWR=vdd prefix=sky130_fd_sc_lp__ }
C {sky130_stdcells/inv_1.sym} 740 300 0 0 {name=x5 VGND=GND VNB=GND VPB=vdd VPWR=vdd prefix=sky130_fd_sc_lp__ }
