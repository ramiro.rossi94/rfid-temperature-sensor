v {xschem version=2.9.9 file_version=1.2 }
G {}
K {}
V {}
S {}
E {}
N -60 130 -60 160 { lab=GND}
N 30 130 30 160 { lab=vss}
N 30 40 30 70 { lab=vdd}
N -60 40 -60 70 { lab=vss}
N 290 60 320 60 { lab=#net1}
N 1230 160 1230 190 { lab=vss}
N 170 60 200 60 { lab=out}
N 240 90 240 120 { lab=vss}
N 240 0 240 30 { lab=vdd}
N 200 70 200 110 { lab=vss}
N 200 110 240 110 { lab=vss}
N 440 60 470 60 { lab=#net2}
N 320 60 350 60 { lab=#net1}
N 390 90 390 120 { lab=vss}
N 390 0 390 30 { lab=vdd}
N 350 70 350 110 { lab=vss}
N 350 110 390 110 { lab=vss}
N 590 60 620 60 { lab=#net3}
N 470 60 500 60 { lab=#net2}
N 540 90 540 120 { lab=vss}
N 540 0 540 30 { lab=vdd}
N 500 70 500 110 { lab=vss}
N 500 110 540 110 { lab=vss}
N 740 60 770 60 { lab=#net4}
N 770 60 800 60 { lab=#net4}
N 840 90 840 120 { lab=vss}
N 840 0 840 30 { lab=vdd}
N 800 70 800 110 { lab=vss}
N 800 110 840 110 { lab=vss}
N 740 60 770 60 { lab=#net4}
N 620 60 650 60 { lab=#net3}
N 690 90 690 120 { lab=vss}
N 690 0 690 30 { lab=vdd}
N 650 70 650 110 { lab=vss}
N 650 110 690 110 { lab=vss}
N 740 60 770 60 { lab=#net4}
N 770 60 800 60 { lab=#net4}
N 840 90 840 120 { lab=vss}
N 840 0 840 30 { lab=vdd}
N 800 70 800 110 { lab=vss}
N 800 110 840 110 { lab=vss}
N 740 60 770 60 { lab=#net4}
N 620 60 650 60 { lab=#net3}
N 690 90 690 120 { lab=vss}
N 690 0 690 30 { lab=vdd}
N 650 70 650 110 { lab=vss}
N 650 110 690 110 { lab=vss}
N 1110 90 1110 120 { lab=vss}
N 1110 0 1110 30 { lab=vdd}
N 1070 70 1070 110 { lab=vss}
N 1070 110 1110 110 { lab=vss}
N 960 90 960 120 { lab=vss}
N 960 0 960 30 { lab=vdd}
N 920 70 920 110 { lab=vss}
N 920 110 960 110 { lab=vss}
N 1110 90 1110 120 { lab=vss}
N 1110 0 1110 30 { lab=vdd}
N 1070 70 1070 110 { lab=vss}
N 1070 110 1110 110 { lab=vss}
N 960 90 960 120 { lab=vss}
N 960 0 960 30 { lab=vdd}
N 920 70 920 110 { lab=vss}
N 920 110 960 110 { lab=vss}
N 1160 60 1230 60 { lab=out}
N 1230 60 1230 100 { lab=out}
N 890 60 920 60 { lab=#net5}
N 1010 60 1070 60 { lab=#net6}
N 1180 60 1180 200 { lab=out}
N 170 200 1180 200 { lab=out}
N 170 60 170 200 { lab=out}
C {../Design/not_a.sym} 240 60 0 0 {name=x1}
C {vsource.sym} -60 100 0 0 {name=V1 value=DC\{Vss\}}
C {vsource.sym} 30 100 0 0 {name=V2 value=DC\{Vdd\}}
C {gnd.sym} -60 160 0 0 {name=l1 lab=GND}
C {lab_pin.sym} -60 40 1 0 {name=l3 sig_type=std_logic lab=vss}
C {lab_pin.sym} 30 160 3 0 {name=l2 sig_type=std_logic lab=vss}
C {lab_pin.sym} 30 40 1 0 {name=l9 sig_type=std_logic lab=vdd
}
C {netlist_not_shown.sym} 260 -140 0 0 {name=SIMULATION only_toplevel=false 

value="


* Circuit Parameters
.param vss  = 0.0
.param vin  = 1.8

* Select corner
.include ~/Projects/rfid-temperature-sensor/ring-oscillator/Testbench/corner_tt.sp

* OP Parameters & Singals to save
.save all
+ @M.X1.XM1.msky130_fd_pr__nfet_01v8[id] @M.X1.XM1.msky130_fd_pr__nfet_01v8[vth] @M.X1.XM1.msky130_fd_pr__nfet_01v8[vgs] @M.X1.XM1.msky130_fd_pr__nfet_01v8[vds] @M.X1.XM1.msky130_fd_pr__nfet_01v8[vdsat] @M.X1.XM1.msky130_fd_pr__nfet_01v8[gm] @M.X1.XM1.msky130_fd_pr__nfet_01v8[gds]
+ @M.X1.XM2.msky130_fd_pr__pfet_01v8[id] @M.X1.XM2.msky130_fd_pr__pfet_01v8[vth] @M.X1.XM2.msky130_fd_pr__pfet_01v8[vgs] @M.X1.XM2.msky130_fd_pr__pfet_01v8[vds] @M.X1.XM2.msky130_fd_pr__pfet_01v8[vdsat] @M.X1.XM2.msky130_fd_pr__pfet_01v8[gm] @M.X1.XM2.msky130_fd_pr__pfet_01v8[gds]

*Simulations
.control
  * Operation Point
  op
  save all

  tran 0.01n 10.5u 10u
  setplot tran1
  plot v(out)
  write ring_osc.raw
  reset

  echo '------------------------ THD ANALYSIS ------------------------'   
  tran 0.01n 10.5u 10u
  plot v(out) title 'Wave Output'
  fourier 1e6 out
  let THD_db = db(fourier11[1][3]/fourier11[1][1])
  print THD_db
  fft out
  plot mag(out)
  plot db(mag(out))

  reset
  
.endc

.end
"}
C {capa.sym} 1230 130 0 0 {name=C1
m=1
value=1p
footprint=1206
device="ceramic capacitor"}
C {lab_pin.sym} 1230 190 3 0 {name=l4 sig_type=std_logic lab=vss}
C {lab_pin.sym} 240 120 3 0 {name=l5 sig_type=std_logic lab=vss}
C {lab_pin.sym} 240 0 1 0 {name=l6 sig_type=std_logic lab=vdd
}
C {lab_pin.sym} 1230 60 2 0 {name=l8 sig_type=std_logic lab=out}
C {../Design/not_a.sym} 390 60 0 0 {name=x2}
C {lab_pin.sym} 390 120 3 0 {name=l10 sig_type=std_logic lab=vss}
C {lab_pin.sym} 390 0 1 0 {name=l11 sig_type=std_logic lab=vdd
}
C {../Design/not_a.sym} 540 60 0 0 {name=x3}
C {lab_pin.sym} 540 120 3 0 {name=l12 sig_type=std_logic lab=vss}
C {lab_pin.sym} 540 0 1 0 {name=l13 sig_type=std_logic lab=vdd
}
C {lab_pin.sym} 840 120 3 0 {name=l16 sig_type=std_logic lab=vss}
C {lab_pin.sym} 840 0 1 0 {name=l17 sig_type=std_logic lab=vdd
}
C {lab_pin.sym} 690 120 3 0 {name=l14 sig_type=std_logic lab=vss}
C {lab_pin.sym} 690 0 1 0 {name=l15 sig_type=std_logic lab=vdd
}
C {../Design/not_a.sym} 840 60 0 0 {name=x6}
C {lab_pin.sym} 840 120 3 0 {name=l18 sig_type=std_logic lab=vss}
C {lab_pin.sym} 840 0 1 0 {name=l19 sig_type=std_logic lab=vdd
}
C {../Design/not_a.sym} 690 60 0 0 {name=x7}
C {lab_pin.sym} 690 120 3 0 {name=l20 sig_type=std_logic lab=vss}
C {lab_pin.sym} 690 0 1 0 {name=l21 sig_type=std_logic lab=vdd
}
C {lab_pin.sym} 1110 120 3 0 {name=l24 sig_type=std_logic lab=vss}
C {lab_pin.sym} 1110 0 1 0 {name=l25 sig_type=std_logic lab=vdd
}
C {lab_pin.sym} 960 120 3 0 {name=l26 sig_type=std_logic lab=vss}
C {lab_pin.sym} 960 0 1 0 {name=l27 sig_type=std_logic lab=vdd
}
C {../Design/not_a.sym} 1110 60 0 0 {name=x10}
C {lab_pin.sym} 1110 120 3 0 {name=l28 sig_type=std_logic lab=vss}
C {lab_pin.sym} 1110 0 1 0 {name=l29 sig_type=std_logic lab=vdd
}
C {../Design/not_a.sym} 960 60 0 0 {name=x11}
C {lab_pin.sym} 960 120 3 0 {name=l30 sig_type=std_logic lab=vss}
C {lab_pin.sym} 960 0 1 0 {name=l31 sig_type=std_logic lab=vdd
}
