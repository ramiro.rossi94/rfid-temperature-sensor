v {xschem version=2.9.9 file_version=1.2 }
G {}
K {}
V {}
S {}
E {}
N 90 320 90 330 { lab=GND}
N 90 210 90 240 { lab=vdd}
N 300 320 320 320 { lab=in}
N 250 320 250 360 { lab=in}
N 300 380 320 380 { lab=in}
N 300 320 300 380 { lab=in}
N 300 440 320 440 { lab=in}
N 300 380 300 440 { lab=in}
N 300 260 320 260 { lab=in}
N 300 260 300 320 { lab=in}
N 300 200 320 200 { lab=in}
N 300 140 320 140 { lab=in}
N 300 200 300 260 { lab=in}
N 250 320 300 320 { lab=in}
N 300 140 300 200 { lab=in}
N 300 80 320 80 { lab=in}
N 300 80 300 140 { lab=in}
N 400 80 440 80 { lab=inv1}
N 400 200 440 200 { lab=inv4}
N 400 260 440 260 { lab=inv6}
N 400 320 440 320 { lab=inv8}
N 400 380 440 380 { lab=inv12}
N 400 440 440 440 { lab=inv16}
N 400 140 440 140 { lab=inv2}
N 520 80 560 80 { lab=#net1}
N 520 200 560 200 { lab=#net2}
N 520 260 560 260 { lab=#net3}
N 520 320 560 320 { lab=#net4}
N 520 380 560 380 { lab=#net5}
N 520 440 560 440 { lab=#net6}
N 520 140 560 140 { lab=#net7}
N 250 420 250 440 { lab=GND}
N 620 440 630 440 { lab=GND}
N 620 380 630 380 { lab=GND}
N 620 320 630 320 { lab=GND}
N 620 260 630 260 { lab=GND}
N 620 200 630 200 { lab=GND}
N 620 140 630 140 { lab=GND}
N 620 80 630 80 { lab=GND}
N 20 210 20 220 { lab=vdd}
N 20 210 90 210 { lab=vdd}
N 20 280 20 320 { lab=GND}
N 20 320 90 320 { lab=GND}
N 90 300 90 320 { lab=GND}
C {vsource.sym} 90 270 0 0 {name=V2 value=DC\{Vdd\}}
C {lab_pin.sym} 90 210 1 0 {name=l9 sig_type=std_logic lab=vdd
}
C {netlist_not_shown.sym} 110 40 0 0 {name=SIMULATION only_toplevel=false 

value="


* Circuit Parameters

* Include Models
.include ~/Projects/rfid-temperature-sensor/ring-oscillator/Testbench/corner_tt.sp
.lib ~/Projects/rfid-temperature-sensor/sky130.lib SCINV

*Simulations
.control
  tran 0.00001n 1.05n 0.995ns
  setplot tran1
  plot v(in) v(inv1) v(inv2) v(inv4) v(inv6) v(inv8) v(inv12) v(inv16)
  
.endc

.end
"}
C {vsource.sym} 250 390 0 1 {name=V3 value="PULSE(0 \{vdd\} 1ns 1ps 1ps 20ns 40ns) DC\{vdd/2\}"}
C {gnd.sym} 90 330 0 0 {name=l2 lab=GND}
C {gnd.sym} 250 440 0 0 {name=l1 lab=GND}
C {sky130_stdcells/inv_1.sym} 360 80 0 0 {name=x1 VGND=GND VNB=GND VPB=vdd VPWR=vdd prefix=sky130_fd_sc_hd__ }
C {lab_pin.sym} 420 80 1 0 {name=l3 sig_type=std_logic lab=inv1}
C {lab_pin.sym} 420 140 1 0 {name=l4 sig_type=std_logic lab=inv2}
C {lab_pin.sym} 420 200 1 0 {name=l5 sig_type=std_logic lab=inv4}
C {lab_pin.sym} 420 260 1 0 {name=l6 sig_type=std_logic lab=inv6}
C {lab_pin.sym} 420 320 1 0 {name=l7 sig_type=std_logic lab=inv8}
C {lab_pin.sym} 420 380 1 0 {name=l8 sig_type=std_logic lab=inv12}
C {lab_pin.sym} 420 440 1 0 {name=l10 sig_type=std_logic lab=inv16}
C {lab_pin.sym} 250 320 1 0 {name=l11 sig_type=std_logic lab=in}
C {res.sym} 590 80 1 0 {name=R1
value=100k
footprint=1206
device=resistor
m=1}
C {res.sym} 590 140 1 0 {name=R2
value=100k
footprint=1206
device=resistor
m=1}
C {res.sym} 590 200 1 0 {name=R3
value=100k
footprint=1206
device=resistor
m=1}
C {res.sym} 590 260 1 0 {name=R4
value=100k
footprint=1206
device=resistor
m=1}
C {res.sym} 590 320 1 0 {name=R5
value=100k
footprint=1206
device=resistor
m=1}
C {res.sym} 590 380 1 0 {name=R6
value=100k
footprint=1206
device=resistor
m=1}
C {res.sym} 590 440 1 0 {name=R7
value=100k
footprint=1206
device=resistor
m=1}
C {gnd.sym} 630 440 3 0 {name=l12 lab=GND}
C {gnd.sym} 630 380 3 0 {name=l13 lab=GND}
C {gnd.sym} 630 320 3 0 {name=l14 lab=GND}
C {gnd.sym} 630 260 3 0 {name=l15 lab=GND}
C {gnd.sym} 630 200 3 0 {name=l16 lab=GND}
C {gnd.sym} 630 140 3 0 {name=l17 lab=GND}
C {gnd.sym} 630 80 3 0 {name=l18 lab=GND}
C {res.sym} 20 250 2 0 {name=R8
value=100k
footprint=1206
device=resistor
m=1}
C {sky130_stdcells/inv_2.sym} 360 140 0 0 {name=x2 VGND=GND VNB=GND VPB=vdd VPWR=vdd prefix=sky130_fd_sc_hd__ }
C {sky130_stdcells/inv_4.sym} 360 200 0 0 {name=x3 VGND=GND VNB=GND VPB=vdd VPWR=vdd prefix=sky130_fd_sc_hd__ }
C {sky130_stdcells/inv_6.sym} 360 260 0 0 {name=x4 VGND=GND VNB=GND VPB=vdd VPWR=vdd prefix=sky130_fd_sc_hd__ }
C {sky130_stdcells/inv_8.sym} 360 320 0 0 {name=x5 VGND=GND VNB=GND VPB=vdd VPWR=vdd prefix=sky130_fd_sc_hd__ }
C {sky130_stdcells/inv_12.sym} 360 380 0 0 {name=x6 VGND=GND VNB=GND VPB=vdd VPWR=vdd prefix=sky130_fd_sc_hd__ }
C {sky130_stdcells/inv_16.sym} 360 440 0 0 {name=x7 VGND=GND VNB=GND VPB=vdd VPWR=vdd prefix=sky130_fd_sc_hd__ }
C {sky130_stdcells/inv_1.sym} 480 80 0 0 {name=x8 VGND=GND VNB=GND VPB=vdd VPWR=vdd prefix=sky130_fd_sc_hd__ }
C {sky130_stdcells/inv_2.sym} 480 140 0 0 {name=x9 VGND=GND VNB=GND VPB=vdd VPWR=vdd prefix=sky130_fd_sc_hd__ }
C {sky130_stdcells/inv_4.sym} 480 200 0 0 {name=x10 VGND=GND VNB=GND VPB=vdd VPWR=vdd prefix=sky130_fd_sc_hd__ }
C {sky130_stdcells/inv_6.sym} 480 260 0 0 {name=x11 VGND=GND VNB=GND VPB=vdd VPWR=vdd prefix=sky130_fd_sc_hd__ }
C {sky130_stdcells/inv_8.sym} 480 320 0 0 {name=x12 VGND=GND VNB=GND VPB=vdd VPWR=vdd prefix=sky130_fd_sc_hd__ }
C {sky130_stdcells/inv_12.sym} 480 380 0 0 {name=x13 VGND=GND VNB=GND VPB=vdd VPWR=vdd prefix=sky130_fd_sc_hd__ }
C {sky130_stdcells/inv_16.sym} 480 440 0 0 {name=x14 VGND=GND VNB=GND VPB=vdd VPWR=vdd prefix=sky130_fd_sc_hd__ }
