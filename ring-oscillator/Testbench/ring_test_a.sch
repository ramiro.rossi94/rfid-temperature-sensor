v {xschem version=2.9.9 file_version=1.2 }
G {}
K {}
V {}
S {}
E {}
N 110 -110 110 -80 { lab=GND}
N 110 -200 110 -170 { lab=vdd}
N 440 10 440 50 { lab=vdd}
N 440 110 440 140 { lab=GND}
N 400 130 440 130 { lab=GND}
N 400 90 400 130 { lab=GND}
N 570 10 570 50 { lab=vdd}
N 570 110 570 140 { lab=GND}
N 530 130 570 130 { lab=GND}
N 530 90 530 130 { lab=GND}
N 490 80 530 80 { lab=#net1}
N 700 10 700 50 { lab=vdd}
N 700 110 700 140 { lab=GND}
N 660 130 700 130 { lab=GND}
N 660 90 660 130 { lab=GND}
N 620 80 660 80 { lab=#net2}
N 830 10 830 50 { lab=vdd}
N 830 110 830 140 { lab=GND}
N 790 130 830 130 { lab=GND}
N 790 90 790 130 { lab=GND}
N 750 80 790 80 { lab=#net3}
N 880 80 920 80 { lab=out}
N 310 10 310 50 { lab=vdd}
N 310 110 310 140 { lab=GND}
N 270 130 310 130 { lab=GND}
N 270 90 270 130 { lab=GND}
N 230 80 270 80 { lab=out}
N 360 80 400 80 { lab=#net4}
N 230 -50 230 80 { lab=out}
N 230 -50 910 -50 { lab=out}
N 910 -50 910 80 { lab=out}
C {vsource.sym} 110 -140 0 0 {name=V2 value=DC\{vdd\}}
C {gnd.sym} 110 -80 0 0 {name=l1 lab=GND}
C {lab_pin.sym} 110 -200 1 0 {name=l9 sig_type=std_logic lab=vdd
}
C {netlist_not_shown.sym} 430 -220 0 0 {name=SIMULATION only_toplevel=false 

value="


* Circuit Parameters
.param CAP  = 2p

* Include Models
.include ~/Projects/rfid-temperature-sensor/ring-oscillator/Testbench/corner_tt.sp

* OP Parameters & Singals to save
.save all

*Simulations
.control
  let minTemp = 0
  let maxTemp = 100

  setplot tran1
  echo 'inv,temp,period1' > '~/Projects/rfid-temperature-sensor/ring-oscillator/Testbench/ring_test_a.csv'
  let cnt=minTemp
  while cnt <= maxTemp
    echo 'TEMP $&cnt'
    reset
    option temp=$&cnt
    tran 0.001n 100n 99n
    *linearize
    plot v(out)
    meas tran period TRIG v(out) VAL=0.9 RISE=1 TARG V(out) VAL=0.9 RISE=2
    set line = 'inv1,$&cnt,$&period'
    echo $line >> '~/Projects/rfid-temperature-sensor/ring-oscillator/Testbench/ring_test_a.csv'
    let cnt = cnt + 10
  end



  *tran 0.1n 920n 900n
  *setplot tran1
  *plot v(osc) v(load)

  *linearize
  *set specwindow=blackman
  *fft v(osc)
  *spec 10 1000000 1000 v(osc)
  *plot mag(v(osc))
  
.endc

.end
"}
C {lab_wire.sym} 920 80 2 0 {name=l23 sig_type=std_logic lab=out}
C {lab_pin.sym} 440 10 1 0 {name=l15 sig_type=std_logic lab=vdd
}
C {../Design/not_a.sym} 570 80 0 0 {name=x3}
C {lab_pin.sym} 570 10 1 0 {name=l5 sig_type=std_logic lab=vdd
}
C {../Design/not_a.sym} 440 80 0 0 {name=x2}
C {lab_pin.sym} 700 10 1 0 {name=l7 sig_type=std_logic lab=vdd
}
C {../Design/not_a.sym} 700 80 0 0 {name=x4}
C {lab_pin.sym} 830 10 1 0 {name=l10 sig_type=std_logic lab=vdd
}
C {../Design/not_a.sym} 830 80 0 0 {name=x5}
C {gnd.sym} 440 140 0 0 {name=l3 lab=GND}
C {gnd.sym} 570 140 0 0 {name=l6 lab=GND}
C {gnd.sym} 700 140 0 0 {name=l8 lab=GND}
C {gnd.sym} 830 140 0 0 {name=l11 lab=GND}
C {lab_pin.sym} 310 10 1 0 {name=l2 sig_type=std_logic lab=vdd
}
C {../Design/not_a.sym} 310 80 0 0 {name=x1}
C {gnd.sym} 310 140 0 0 {name=l4 lab=GND}
