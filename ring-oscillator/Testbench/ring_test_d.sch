v {xschem version=2.9.9 file_version=1.2 }
G {}
K {}
V {}
S {}
E {}
N 80 640 80 670 { lab=GND}
N 80 550 80 580 { lab=vdd}
N 840 80 870 80 { lab=inv1}
N 870 80 880 80 { lab=inv1}
N 360 80 400 80 { lab=#net1}
N 480 80 520 80 { lab=#net2}
N 600 80 640 80 { lab=#net3}
N 720 80 760 80 { lab=#net4}
N 240 80 280 80 { lab=inv1}
N 240 20 240 80 { lab=inv1}
N 240 20 880 20 { lab=inv1}
N 880 20 880 80 { lab=inv1}
N 840 200 870 200 { lab=inv2}
N 870 200 880 200 { lab=inv2}
N 360 200 400 200 { lab=#net5}
N 480 200 520 200 { lab=#net6}
N 600 200 640 200 { lab=#net7}
N 720 200 760 200 { lab=#net8}
N 240 200 280 200 { lab=inv2}
N 240 140 240 200 { lab=inv2}
N 240 140 880 140 { lab=inv2}
N 880 140 880 200 { lab=inv2}
N 840 320 870 320 { lab=inv4}
N 870 320 880 320 { lab=inv4}
N 360 320 400 320 { lab=#net9}
N 480 320 520 320 { lab=#net10}
N 600 320 640 320 { lab=#net11}
N 720 320 760 320 { lab=#net12}
N 240 320 280 320 { lab=inv4}
N 240 260 240 320 { lab=inv4}
N 240 260 880 260 { lab=inv4}
N 880 260 880 320 { lab=inv4}
N 840 440 870 440 { lab=inv6}
N 870 440 880 440 { lab=inv6}
N 360 440 400 440 { lab=#net13}
N 480 440 520 440 { lab=#net14}
N 600 440 640 440 { lab=#net15}
N 720 440 760 440 { lab=#net16}
N 240 440 280 440 { lab=inv6}
N 240 380 240 440 { lab=inv6}
N 240 380 880 380 { lab=inv6}
N 880 380 880 440 { lab=inv6}
N 840 560 870 560 { lab=inv8}
N 870 560 880 560 { lab=inv8}
N 360 560 400 560 { lab=#net17}
N 480 560 520 560 { lab=#net18}
N 600 560 640 560 { lab=#net19}
N 720 560 760 560 { lab=#net20}
N 240 560 280 560 { lab=inv8}
N 240 500 240 560 { lab=inv8}
N 240 500 880 500 { lab=inv8}
N 880 500 880 560 { lab=inv8}
N 840 680 870 680 { lab=inv12}
N 870 680 880 680 { lab=inv12}
N 360 680 400 680 { lab=#net21}
N 480 680 520 680 { lab=#net22}
N 600 680 640 680 { lab=#net23}
N 720 680 760 680 { lab=#net24}
N 240 680 280 680 { lab=inv12}
N 240 620 240 680 { lab=inv12}
N 240 620 880 620 { lab=inv12}
N 880 620 880 680 { lab=inv12}
N 840 800 870 800 { lab=inv16}
N 870 800 880 800 { lab=inv16}
N 360 800 400 800 { lab=#net25}
N 480 800 520 800 { lab=#net26}
N 600 800 640 800 { lab=#net27}
N 720 800 760 800 { lab=#net28}
N 240 800 280 800 { lab=inv16}
N 240 740 240 800 { lab=inv16}
N 240 740 880 740 { lab=inv16}
N 880 740 880 800 { lab=inv16}
C {vsource.sym} 80 610 0 0 {name=V2 value=DC\{VDD\}}
C {gnd.sym} 80 670 0 0 {name=l1 lab=GND}
C {lab_pin.sym} 80 550 1 0 {name=l9 sig_type=std_logic lab=vdd
}
C {netlist_not_shown.sym} 10 190 0 0 {name=SIMULATION only_toplevel=false 

value="


* Circuit Parameters

* Include Models
.include ~/Projects/rfid-temperature-sensor/ring-oscillator/Testbench/corner_tt.sp
.lib ~/Projects/rfid-temperature-sensor/sky130.lib SCINV

* OP Parameters & Singals to save
.save all

*Simulations
.control
  tran 0.01n 9.5n 9n
  setplot tran1
  plot v(inv1) v(inv2) v(inv4) v(inv6) v(inv8) v(inv12) v(inv16)

  *linearize
  *set specwindow=blackman
  *fft v(osc)
  *spec 10 1000000 1000 v(load)
  *plot mag(v(load))
  
.endc

.end
"}
C {lab_pin.sym} 880 60 2 0 {name=l20 sig_type=std_logic lab=inv1}
C {sky130_stdcells/inv_1.sym} 320 80 0 0 {name=x1 VGND=GND VNB=GND VPB=vdd VPWR=vdd prefix=sky130_fd_sc_hd__ }
C {sky130_stdcells/inv_1.sym} 440 80 0 0 {name=x2 VGND=GND VNB=GND VPB=vdd VPWR=vdd prefix=sky130_fd_sc_hd__ }
C {sky130_stdcells/inv_1.sym} 560 80 0 0 {name=x3 VGND=GND VNB=GND VPB=vdd VPWR=vdd prefix=sky130_fd_sc_hd__ }
C {sky130_stdcells/inv_1.sym} 680 80 0 0 {name=x4 VGND=GND VNB=GND VPB=vdd VPWR=vdd prefix=sky130_fd_sc_hd__ }
C {sky130_stdcells/inv_1.sym} 800 80 0 0 {name=x5 VGND=GND VNB=GND VPB=vdd VPWR=vdd prefix=sky130_fd_sc_hd__ }
C {lab_pin.sym} 880 180 2 0 {name=l2 sig_type=std_logic lab=inv2}
C {sky130_stdcells/inv_2.sym} 320 200 0 0 {name=x6 VGND=GND VNB=GND VPB=vdd VPWR=vdd prefix=sky130_fd_sc_hd__ }
C {sky130_stdcells/inv_2.sym} 440 200 0 0 {name=x7 VGND=GND VNB=GND VPB=vdd VPWR=vdd prefix=sky130_fd_sc_hd__ }
C {sky130_stdcells/inv_2.sym} 560 200 0 0 {name=x8 VGND=GND VNB=GND VPB=vdd VPWR=vdd prefix=sky130_fd_sc_hd__ }
C {sky130_stdcells/inv_2.sym} 680 200 0 0 {name=x9 VGND=GND VNB=GND VPB=vdd VPWR=vdd prefix=sky130_fd_sc_hd__ }
C {sky130_stdcells/inv_2.sym} 800 200 0 0 {name=x10 VGND=GND VNB=GND VPB=vdd VPWR=vdd prefix=sky130_fd_sc_hd__ }
C {lab_pin.sym} 880 300 2 0 {name=l3 sig_type=std_logic lab=inv4}
C {sky130_stdcells/inv_4.sym} 320 320 0 0 {name=x11 VGND=GND VNB=GND VPB=vdd VPWR=vdd prefix=sky130_fd_sc_hd__ }
C {lab_pin.sym} 880 420 2 0 {name=l4 sig_type=std_logic lab=inv6}
C {sky130_stdcells/inv_6.sym} 320 440 0 0 {name=x16 VGND=GND VNB=GND VPB=vdd VPWR=vdd prefix=sky130_fd_sc_hd__ }
C {lab_pin.sym} 880 540 2 0 {name=l5 sig_type=std_logic lab=inv8}
C {sky130_stdcells/inv_8.sym} 320 560 0 0 {name=x21 VGND=GND VNB=GND VPB=vdd VPWR=vdd prefix=sky130_fd_sc_hd__ }
C {lab_pin.sym} 880 660 2 0 {name=l6 sig_type=std_logic lab=inv12}
C {sky130_stdcells/inv_12.sym} 320 680 0 0 {name=x26 VGND=GND VNB=GND VPB=vdd VPWR=vdd prefix=sky130_fd_sc_hd__ }
C {lab_pin.sym} 880 780 2 0 {name=l7 sig_type=std_logic lab=inv16}
C {sky130_stdcells/inv_16.sym} 320 800 0 0 {name=x31 VGND=GND VNB=GND VPB=vdd VPWR=vdd prefix=sky130_fd_sc_hd__ }
C {sky130_stdcells/inv_4.sym} 440 320 0 0 {name=x12 VGND=GND VNB=GND VPB=vdd VPWR=vdd prefix=sky130_fd_sc_hd__ }
C {sky130_stdcells/inv_4.sym} 560 320 0 0 {name=x13 VGND=GND VNB=GND VPB=vdd VPWR=vdd prefix=sky130_fd_sc_hd__ }
C {sky130_stdcells/inv_4.sym} 680 320 0 0 {name=x14 VGND=GND VNB=GND VPB=vdd VPWR=vdd prefix=sky130_fd_sc_hd__ }
C {sky130_stdcells/inv_4.sym} 800 320 0 0 {name=x15 VGND=GND VNB=GND VPB=vdd VPWR=vdd prefix=sky130_fd_sc_hd__ }
C {sky130_stdcells/inv_6.sym} 440 440 0 0 {name=x17 VGND=GND VNB=GND VPB=vdd VPWR=vdd prefix=sky130_fd_sc_hd__ }
C {sky130_stdcells/inv_6.sym} 560 440 0 0 {name=x18 VGND=GND VNB=GND VPB=vdd VPWR=vdd prefix=sky130_fd_sc_hd__ }
C {sky130_stdcells/inv_6.sym} 680 440 0 0 {name=x19 VGND=GND VNB=GND VPB=vdd VPWR=vdd prefix=sky130_fd_sc_hd__ }
C {sky130_stdcells/inv_6.sym} 800 440 0 0 {name=x20 VGND=GND VNB=GND VPB=vdd VPWR=vdd prefix=sky130_fd_sc_hd__ }
C {sky130_stdcells/inv_8.sym} 440 560 0 0 {name=x22 VGND=GND VNB=GND VPB=vdd VPWR=vdd prefix=sky130_fd_sc_hd__ }
C {sky130_stdcells/inv_8.sym} 560 560 0 0 {name=x23 VGND=GND VNB=GND VPB=vdd VPWR=vdd prefix=sky130_fd_sc_hd__ }
C {sky130_stdcells/inv_8.sym} 680 560 0 0 {name=x24 VGND=GND VNB=GND VPB=vdd VPWR=vdd prefix=sky130_fd_sc_hd__ }
C {sky130_stdcells/inv_8.sym} 800 560 0 0 {name=x25 VGND=GND VNB=GND VPB=vdd VPWR=vdd prefix=sky130_fd_sc_hd__ }
C {sky130_stdcells/inv_12.sym} 440 680 0 0 {name=x27 VGND=GND VNB=GND VPB=vdd VPWR=vdd prefix=sky130_fd_sc_hd__ }
C {sky130_stdcells/inv_12.sym} 560 680 0 0 {name=x28 VGND=GND VNB=GND VPB=vdd VPWR=vdd prefix=sky130_fd_sc_hd__ }
C {sky130_stdcells/inv_12.sym} 680 680 0 0 {name=x29 VGND=GND VNB=GND VPB=vdd VPWR=vdd prefix=sky130_fd_sc_hd__ }
C {sky130_stdcells/inv_12.sym} 800 680 0 0 {name=x30 VGND=GND VNB=GND VPB=vdd VPWR=vdd prefix=sky130_fd_sc_hd__ }
C {sky130_stdcells/inv_16.sym} 440 800 0 0 {name=x32 VGND=GND VNB=GND VPB=vdd VPWR=vdd prefix=sky130_fd_sc_hd__ }
C {sky130_stdcells/inv_16.sym} 560 800 0 0 {name=x33 VGND=GND VNB=GND VPB=vdd VPWR=vdd prefix=sky130_fd_sc_hd__ }
C {sky130_stdcells/inv_16.sym} 680 800 0 0 {name=x34 VGND=GND VNB=GND VPB=vdd VPWR=vdd prefix=sky130_fd_sc_hd__ }
C {sky130_stdcells/inv_16.sym} 800 800 0 0 {name=x35 VGND=GND VNB=GND VPB=vdd VPWR=vdd prefix=sky130_fd_sc_hd__ }
