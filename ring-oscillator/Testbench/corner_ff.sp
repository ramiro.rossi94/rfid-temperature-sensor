* PARAMETERS FOR CORNER FF
.param vdd = 1.98
.param iref = 105u
.options TEMP = 0.0

* Fabrication process
.lib ~/Projects/rfid-temperature-sensor/sky130.lib FF
