`timescale 1ns / 1ps

//////////////////////////////////////////////////////////////////////////////////

// Logica digital para transceptor de RFID
// Module Name:  digital_RFID
// Project Name: digital_RFID

// Ramiro Gonzalez
// UTN FRBA
// Mayo 2017
//

/*

	Este es el Dise�o 1. Tiene:
					 |--->	portadora
	- 3 entradas: ---|--->	muestra (llega desde el demodulador)
					 |--->	reset
	- 2 salidas identicas: se�al que se env�a hacia el modulador.

*/

//////////////////////////////////////////////////////////////////////////////////

module digital_RFID(

	input  wire entrada_muestra	,
	//output wire portadora		,
	input  wire portadora		,
	output wire resetn			, //delta1, delta2,
	output wire salida_modulador,
	output wire salida_auxiliar	,
	output wire led_reset
   );

	wire   reset;
	assign led_reset = reset;
	assign reset 	 = ~resetn;

// Constantes y registro para trabajar con Fc = 13,56 / 4 (MHz)
localparam	INICIO_DEMORA	= 9'b10;			// Valor inicial 2 xq:	1) Mantengo la pausa durante 1 pulsos de portadora.
												//						2) El demodulador tiene una demora para cambiar de estado.
localparam	FINAL_DEMORA_1	= 9'b1_0011_0011;	// FINAL 1 = (1236/4-2)/fc = 307	-> Le resto 1 xq primero cumple el periodo y luego incrementa el contador.
localparam	FINAL_DEMORA_0	= 9'b1_0010_0011;	// FINAL 0 = (1172/4-2)/fc = 291	-> Le resto otro 1 xq tarda ese tiempo en activarse la escritura.
reg [8:0] cuenta_demora;						// Contador para calcular el FDT.

// Registros de deteccion y decodificacion:
reg clk_traduccion;			// Se activa cuando decodifico un bit. Ojo que tmb es salida en este module. No es un clock, tengo que ponerlo en cero una vez leido.
reg detecta_EOF;
reg comenzar_escritura;
reg ultimo_bit_demora;
reg reset_meta;
reg reset_sinc;
reg pausa_meta;
reg pausa_sinc;

// Registros de lectura de instrucciones:
reg [6:0]  	cuenta_bit;
reg [80:0] 	lectura_bit;
reg 		REQA_leido;
reg 		enviar_SAK;
reg 		instruccion_leida;

// Registros de habilitacion de etapas de lectura y escritura:
reg habilita_lectura;
reg habilita_escritura;

// Registros para habilitar instrucciones de respuesta:
reg manda_REQ;
reg manda_READ_x;
reg manda_WRITE;
reg manda_SAK;
reg manda_NACK;
reg enviando_respuesta;

// Registro que indica que ya se envio la respuesta y puede volver a leer.
reg rta_enviada;

// Registros de maquina de estados
reg [1:0] 	estado_picc;
reg 		HLTA_leido;
reg 		READ_x_leido;
reg 		WRITE_leido;
localparam 	OCIOSO		= 2'b00;
localparam 	PREPARADO	= 2'b01;
localparam 	ACTIVADO	= 2'b11;
localparam 	BLOQUEADO	= 2'b10;


// Este always sincroniza la se�al de reset.
	always@(posedge portadora, posedge reset)
	begin: SINCRONIZADOR_RESET
		if (reset)
			{reset_sinc,reset_meta}	<= 2'b11;
		else begin
			reset_sinc	<= reset_meta;
			reset_meta	<= 1'b0;
		end

	end

// Este always detecta la pausa y la sincroniza por un s�lo per�odo de clock.
	always@(posedge portadora, negedge entrada_muestra)
	begin: SINCRONIZADOR_PAUSA
		if (!entrada_muestra)
			{pausa_sinc, pausa_meta}	<= 2'b11;
		else begin
			pausa_sinc	<= pausa_meta;
			pausa_meta	<= 1'b0;
		end

	end

// Este always deshabilita la escritura cuando el contador comenzar_escritura llega al final y ninguna de los mensajes
//		a enviar se activa.

	always@(posedge portadora)
	begin: HABILITADOR_ESCRITURA
		case({rta_enviada, reset_sinc, comenzar_escritura, enviando_respuesta, habilita_escritura})
			5'b00000:	habilita_escritura	<= 1'b0;
			5'b00010:	habilita_escritura	<= 1'b0;
			5'b00001:	habilita_escritura	<= 1'b0;
			5'b00011:	habilita_escritura	<= 1'b1;
			5'b00100:	habilita_escritura	<= 1'b0;
			5'b00101:	habilita_escritura	<= 1'b0;
			5'b00110:	habilita_escritura	<= 1'b1;
			5'b00111:	habilita_escritura	<= 1'b1;
			default:	habilita_escritura	<= 1'b0;
		endcase
	end

	reg anticolision_activado;
	reg manda_anticolision;
	reg extiende_cuenta;
	reg [15:0] CRC_corroborar;

//Este always recibe que se termino de leer una instruccion y marca si hay o no respuesta dependiendo deque se haya leido.
	always@(posedge portadora)
	begin: DETECTOR_INSTRUCCION

		case( {rta_enviada, reset_sinc, instruccion_leida, anticolision_activado} )	// ,REQA_leido,enviar_SAK,anticolision_activado
			4'b0000:	{manda_NACK, CRC_corroborar, manda_READ_x, manda_WRITE, manda_REQ, manda_SAK, enviando_respuesta, manda_anticolision, extiende_cuenta}	<= {manda_NACK, CRC_corroborar, manda_READ_x, manda_WRITE, manda_REQ, manda_SAK, enviando_respuesta, manda_anticolision, extiende_cuenta};
			4'b0001:	{manda_NACK, CRC_corroborar, manda_READ_x, manda_WRITE, manda_REQ, manda_SAK, enviando_respuesta, manda_anticolision, extiende_cuenta}	<= {manda_NACK, CRC_corroborar, manda_READ_x, manda_WRITE, manda_REQ, manda_SAK, enviando_respuesta, manda_anticolision, extiende_cuenta};

			4'b0010:	{manda_NACK, CRC_corroborar, manda_READ_x, manda_WRITE, manda_REQ, manda_SAK, enviando_respuesta, manda_anticolision, extiende_cuenta}	<= {enviar_NACK_paridad, CRC_leido, READ_x_leido, WRITE_leido, REQA_leido, enviar_SAK, 1'b1, anticolision_activado, extiende_cuenta};
			4'b0011:	{manda_NACK, CRC_corroborar, manda_READ_x, manda_WRITE, manda_REQ, manda_SAK, enviando_respuesta, manda_anticolision, extiende_cuenta}	<= {enviar_NACK_paridad, CRC_leido, READ_x_leido, WRITE_leido, REQA_leido, enviar_SAK, 1'b1, anticolision_activado, ~lectura_bit[0]};

			default:{manda_NACK, CRC_corroborar, manda_READ_x, manda_WRITE, manda_REQ, manda_SAK, enviando_respuesta, manda_anticolision, extiende_cuenta}	<= 24'b0;
		endcase
	end

// Este always deshabilita la lectura cuando detecto EOF y habilita cuando ocurre una pausa y no se estaba enviando nada. Busco el SOF.
	always@(posedge portadora)
	begin: HABILITADOR_LECTURA

		if ( pausa_sinc || reset_sinc ) begin
			habilita_lectura	<= ~enviando_respuesta;
			ultimo_bit_demora	<= 1'b0;
		end

		else begin
			case ( {detecta_EOF, habilita_lectura} )
				2'b00:	{habilita_lectura, ultimo_bit_demora}	<= {1'b0, ultimo_bit_demora};
				2'b01:	{habilita_lectura, ultimo_bit_demora}	<= {1'b1, ultimo_bit_demora};
				2'b10:	{habilita_lectura, ultimo_bit_demora}	<= {1'b0, lectura_bit[0]};
				2'b11:	{habilita_lectura, ultimo_bit_demora}	<= {1'b0, lectura_bit[0]};
				default:{habilita_lectura, ultimo_bit_demora}	<= {1'b0, 1'b0};
			endcase
		end

	end

// Este always cuenta el tiempo desde la ultima pausa. **************************************++
// Activa "comenzar_escritura" cuando se cumple.
	always@(posedge portadora)
	begin: CONTADOR_DEMORA

		case ( {pausa_sinc, reset_sinc, habilita_escritura, ultimo_bit_demora} )
			4'b0000:	if ( cuenta_demora == FINAL_DEMORA_0 )				comenzar_escritura	<= 1'b1;
						else												cuenta_demora		<= cuenta_demora + 9'b1;
			4'b0001:	if ( cuenta_demora == FINAL_DEMORA_1 )				comenzar_escritura	<= 1'b1;
						else												cuenta_demora		<= cuenta_demora + 9'b1;
			default:	{comenzar_escritura, cuenta_demora}	<= {1'b0, INICIO_DEMORA};
		endcase

	end


// Constantes y registro para trabajar con Fc = 13,56/4 (MHz)
	localparam LIMITE_EOF			= 7'b100_0000; // Cuenta 2 periodos de simbolo	= 256/4 = 64
	localparam LIMITE_3				= 7'b011_0000; // Cuenta 1,5 periodos de simbolo= 192/4 = 48
	localparam LIMITE_2				= 7'b010_0000; // Cuenta 1 periodo de simbolo	= 128/4 = 32
	localparam INICIO_TRADUCCION	= 7'b10;	// Arranca en 2 xq

	reg [6:0] cuenta_traduccion;

	reg [1:0] traduccion;


// Este always cuenta la cantidad de pulsos de portadora hasta ocurrida la pausa. Segun el tiempo transcurrido traduce.
// Tambien detecta si ocurri� algun EOF con "detecta_EOF".
	always@(posedge portadora)
	begin: DECODIFICADOR

		if ( reset_sinc || !habilita_lectura ) begin
			traduccion			<= 2'b00;
			cuenta_traduccion	<= INICIO_TRADUCCION;
			detecta_EOF			<= 1'b0;
			clk_traduccion		<= 1'b0;
		end


		else if (pausa_sinc) begin
			if ( cuenta_traduccion < 9'b100 ) begin	// Este IF protege la reentrancia ya que la pausa_sinc se mantiene
				traduccion		<= traduccion;		//	durante 2 pulsos de clk.
				clk_traduccion	<= 1'b0;
			end
			else begin
				if		(cuenta_traduccion >= LIMITE_EOF)	traduccion	<= 2'b00;
				//	Esta ser�a la traduccion del SOF pero no ocurre nunca. Por eso el SOF ya no se traduce.
				else if	(cuenta_traduccion >= LIMITE_3)		traduccion	<= 2'b11;
				else if	(cuenta_traduccion >= LIMITE_2)		traduccion	<= 2'b10;
				else										traduccion	<= 2'b01;
				clk_traduccion		<= 1'b1;
			end

			cuenta_traduccion	<= INICIO_TRADUCCION;
			detecta_EOF			<= 1'b0;
		end

		else begin
			clk_traduccion	<= 1'b0;
			if (cuenta_traduccion >= LIMITE_EOF)
				detecta_EOF			<= 1'b1;
			else begin
				detecta_EOF			<= 1'b0;
				cuenta_traduccion	<= cuenta_traduccion + 7'b1;	// Para Fc/4
			end
		end

	end

// *********************************|~~~~~~~~~~~|***************************************************************
// *********************************| 2da Parte |***************************************************************
// *********************************|~~~~~~~~~~~|***************************************************************
// Esta parte se encarga de las lecturas de los datos que van llegando. La informacion ya esta decodificada.

	localparam VACIO_INSTRUCCION		= 81'b0;
	localparam INICIAL_INSTRUCCION		= 7'b000_0000;
	localparam FINAL_INSTRUCCION_REQ1	= 7'b000_1000;	// Cuenta hasta 8.
	localparam FINAL_INSTRUCCION_WUP1	= 7'b000_0111;	// Cuenta hasta 7.
	localparam FINAL_INSTRUCCION_HLT1	= 7'b010_0101;	// Cuenta hasta 37.
	localparam FINAL_INSTRUCCION_READ1	= 7'b010_0100;	// Cuenta hasta 36.	-> La funcion READ tiene dos largos:	Si el utlimo bit es 1, entonces el EOF no se guarda.
	localparam FINAL_INSTRUCCION_READ2	= 7'b010_0101;	// Cuenta hasta 37.				-> Si el ultimo bit es 0, entonces el EOF se guarda como un cero extra a la trama enviada.
	localparam FINAL_INSTRUCCION_WRITE1	= 7'b100_1000;	// Cuenta hasta 72.	-> Misma logica que con READ
	localparam FINAL_INSTRUCCION_WRITE2	= 7'b100_1001;	// Cuenta hasta 73.
	localparam FINAL_INSTRUCCION_SEL		= 7'b000_1001;	// Cuenta hasta 9.
	localparam FINAL_INSTRUCCION_NVB1	= 7'b001_0001;	// Cuenta hasta 17.
	localparam FINAL_INSTRUCCION_NVB2	= 7'b001_0010;	// Cuenta hasta 18.
	localparam FINAL_MINIMO_ANTICOLISION= 7'b001_0010;	// Cuenta hasta 18.	-> Aunque la instruccion tiene 17 bits.
	localparam FINAL_INSTRUCCION_MAX2	= 7'b101_0001;	// Cuenta hasta 81.


// Estas son las secuencias de bits que se esperan recibir. El SOF no lo leo por eso no lo tengo en cuenta.
	localparam INSTRUCCION_READ_X			= 8'b0000_1100;														// = READ (30h)
	localparam INSTRUCCION_WRITE			= 8'b0100_0101;														// = WRITE (A2h)
	localparam INSTRUCCION_REQA			= 81'b0110_010_0;						// = REQA			010_0110 (26h) + EOF (0).
	localparam INSTRUCCION_WUPA			= 81'b0100_101;						// = WUPA			101_0010 (52h).
	localparam INSTRUCCION_SEL			= 81'b1100_1001_1;						// = SEL				1001_0011 (93h).
	localparam INSTRUCCION_MAX = 81'b1100_1001_1_0000_1110_0_1000_0000_0_0100_0000_0_0010_0000_0_0001_0000_0_1111_0000_1_0001_1111_0_0111_1101_1;
	localparam INSTRUCCION_HLTA = 81'b0000_1010_1_0000_0000_1_1110_1010_0_1011_0011_0_0;	// = HLTA1 0101_0000 (50h) + HLTA2 (00h) + CRC1 0101_0111 (57h) + CRC2 1100_1101 (CD) + EOF (0).

	localparam FINAL_ANTICOLISION	= 7'b011_1111;		// Cuenta hasta 63.
	localparam bits_UID_COMPLETO = 45'b1000_0000_0_0100_0000_0_0010_0000_0_0001_0000_0_1111_0000_1; // UID + BCC

	reg [6:0] 	cuenta_anticolision;
	reg [6:0] 	cuenta_bits_leidos;
	reg [44:0] 	escritura_auxiliar;
	reg [44:0] 	escritura_completa;

	always@(posedge portadora)
	begin: RESPUESTA_ANTICOLISION

		case( {rta_enviada, reset_sinc, ~habilita_lectura, manda_anticolision} )
			4'b0000:	cuenta_bits_leidos	<= cuenta_bit;
			4'b0001:	cuenta_bits_leidos	<= cuenta_bit;
			4'b0010:	cuenta_bits_leidos	<= cuenta_bits_leidos;
			4'b0011: begin
				if ( cuenta_anticolision+cuenta_bits_leidos < FINAL_ANTICOLISION+extiende_cuenta ) begin	// Que la cantidad de bits copiados y leidos no supere 63.
					escritura_auxiliar[43:0]	<= escritura_auxiliar[44:1];
					escritura_auxiliar[44]		<= escritura_completa[0];
					escritura_completa[43:0]	<= escritura_completa[44:1];
					escritura_completa[44]		<= 1'b0;					//	Esto lo puse xq sino tira warnings el programa
					cuenta_anticolision			<= cuenta_anticolision + 7'b1;
				end
			end
			default: begin
				cuenta_anticolision		<= 7'b0;
				escritura_auxiliar		<= 45'b0;
				escritura_completa		<= bits_UID_COMPLETO;
				cuenta_bits_leidos		<= 7'b0;
			end
		endcase

	end


	reg [15:0] 	registros_CRC;
	reg [7:0] 	cuenta_CRC;
	reg [127:0] entrada_CRC;
	reg [15:0] 	registros_CRC_2;
	reg [4:0] 	cuenta_CRC_2;
	reg [15:0] 	entrada_CRC_2;
	reg [47:0] 	entrada_CRC_3;
	reg 		CRC_corroborado_OK;
	localparam 	realimentacion	= 15'b0001_0000_0010_000;	// x^16+x^12+x^5+1
	localparam 	FINAL_CRC			= 8'b1000_0000;				// Cuenta hasta 128 que son los 16 bytes de informacion de respuesta del comando READ
	localparam 	FINAL_CRC_2		= 5'b1_0000;					// Cuenta hasta 16
	localparam 	FINAL_CRC_3		= 8'b0011_0000;				// Cuenta hasta 48

	always@(posedge portadora)
	begin: MODULO_CRC
		if (reset_sinc || habilita_lectura || rta_enviada ) begin
			registros_CRC		<= 16'b1100_0110_1100_0110;
			cuenta_CRC			<= 8'b1111_1111;
			entrada_CRC			<= 128'b0;
			entrada_CRC_2		<= 16'b0;
			entrada_CRC_3		<= 48'b0;
			registros_CRC_2		<= 16'b1100_0110_1100_0110;
			cuenta_CRC_2		<= 5'b0;
			CRC_corroborado_OK	<= 1'b0;
		end
		else if (READ_x_leido)
				entrada_CRC_2				<= {INSTRUCCION_READ_X, bloque_leido[0], bloque_leido[1], bloque_leido[2], bloque_leido[3], 4'b0};
		else if (WRITE_leido) begin
				entrada_CRC_3[47:32]		<= {INSTRUCCION_WRITE, bloque_leido[0], bloque_leido[1], bloque_leido[2], bloque_leido[3], 4'b0};
				entrada_CRC_3[31:0]		<= {datos_WRITE[35:28], datos_WRITE[26:19], datos_WRITE[17:10], datos_WRITE[8:1]};
		end
		else if (manda_READ_x) begin
			if (cuenta_CRC == 8'b1111_1111) begin
				entrada_CRC[127:96]		<= {READ_memoria[143:136],	READ_memoria[134:127],	READ_memoria[125:118],	READ_memoria[116:109]};
				entrada_CRC[95:64]		<= {READ_memoria[107:100],	READ_memoria[98:91],		READ_memoria[89:82],		READ_memoria[80:73]};
				entrada_CRC[63:32]		<= {READ_memoria[71:64],	READ_memoria[62:55],		READ_memoria[53:46],		READ_memoria[44:37]};
				entrada_CRC[31:0]			<= {READ_memoria[35:28],	READ_memoria[26:19],		READ_memoria[17:10],		READ_memoria[8:1]};
				cuenta_CRC					<= 8'b0;
			end
			else begin
				if (cuenta_CRC < FINAL_CRC) begin
					if (registros_CRC[15]^entrada_CRC[127])
						registros_CRC[15:0]	<= {registros_CRC[14:0]^realimentacion, 1'b1};
					else
						registros_CRC[15:0]	<= {registros_CRC[14:0], 1'b0};
					entrada_CRC[127:1]	<= entrada_CRC[126:0];
					cuenta_CRC				<= cuenta_CRC + 8'b1;
				end
				else begin
					registros_CRC	<= registros_CRC;
					cuenta_CRC		<= cuenta_CRC;
					entrada_CRC		<= entrada_CRC;
				end
				if (cuenta_CRC_2 < FINAL_CRC_2) begin
					if (registros_CRC_2[15]^entrada_CRC_2[15])
						registros_CRC_2[15:0]	<= {registros_CRC_2[14:0]^realimentacion, 1'b1};
					else
						registros_CRC_2[15:0]	<= {registros_CRC_2[14:0], 1'b0};
					entrada_CRC_2[15:1]	<= entrada_CRC_2[14:0];
					entrada_CRC_2[0]		<= 1'b1;	// sino tira warning
					cuenta_CRC_2			<= cuenta_CRC_2 + 5'b1;
				end
				else begin
					registros_CRC_2	<= registros_CRC_2;
					cuenta_CRC_2		<= cuenta_CRC_2;
					entrada_CRC_2		<= entrada_CRC_2;
					if (CRC_corroborar == registros_CRC_2)
						CRC_corroborado_OK	<= 1'b1;
					else
						CRC_corroborado_OK	<= 1'b0;
				end
			end
		end // cierra else if (manda_READ_x)

		else if (manda_WRITE) begin
			if (cuenta_CRC == 8'b1111_1111)
				cuenta_CRC					<= 8'b0;
			else begin
				if (cuenta_CRC < FINAL_CRC_3) begin
					if (registros_CRC[15]^entrada_CRC_3[47])
						registros_CRC[15:0]	<= {registros_CRC[14:0]^realimentacion, 1'b1};
					else
						registros_CRC[15:0]	<= {registros_CRC[14:0], 1'b0};
					entrada_CRC_3[47:1]	<= entrada_CRC_3[46:0];
					//entrada_CRC_3[0]		<= 1'b1;	// sino tira warning
					cuenta_CRC				<= cuenta_CRC + 8'b1;
				end
				else begin
					registros_CRC	<= registros_CRC;
					cuenta_CRC		<= cuenta_CRC;
					entrada_CRC_3	<= entrada_CRC_3;
					if (CRC_corroborar == registros_CRC)
						CRC_corroborado_OK	<= 1'b1;
					else
						CRC_corroborado_OK	<= 1'b0;
				end
			end
		end
	end

// En este always entra cada uno de los bits decodificados y pregunta para 3 largos de registros diferentes si es alguna de las 3 instrucciones posibles.
// En caso de encontrar alguna de las instrucciones, activa uno de los 3 registros para informar cual fue.
// Esta doblemente bloqueado porque deshabilita lectura tambien bloquea clk_traduccion.

	reg [80:0]	lectura_auxiliar;
	reg [80:0]	lectura_completa;
	reg [80:0]	lectura_bit_desplazada;
	reg [80:0]	lectura_auxiliar_desplazada;
	reg [80:0]	lectura_auxiliar_desplazada2;
	reg [3:0]	bloque_leido;
	reg [35:0]	datos_WRITE;
	reg [15:0]	CRC_leido;
	reg 		enviar_NACK_paridad;

	reg sel_detectado;

	always@(posedge portadora)
	begin: LECTOR_BITS

		if( !habilita_lectura || reset_sinc ) begin
			lectura_bit						<= VACIO_INSTRUCCION;
			REQA_leido						<= 1'b0;
			enviar_SAK						<= 1'b0;
			cuenta_bit						<= INICIAL_INSTRUCCION;
			instruccion_leida				<= 1'b0;
			anticolision_activado			<= 1'b0;
			lectura_auxiliar				<= VACIO_INSTRUCCION;
			lectura_completa				<= INSTRUCCION_MAX;
			HLTA_leido						<= 1'b0;
			READ_x_leido					<= 1'b0;
			WRITE_leido						<= 1'b0;
			datos_WRITE						<= 36'b0;
			bloque_leido					<= 4'b1111;
			lectura_bit_desplazada			<= VACIO_INSTRUCCION;
			lectura_auxiliar_desplazada		<= VACIO_INSTRUCCION;
			lectura_auxiliar_desplazada2	<= 81'b1;
			sel_detectado					<= 1'b0;
			CRC_leido						<= 16'b0;
			enviar_NACK_paridad				<= 1'b0;
		end

// Empieza maquina de estado con ifes ********************

		else if ( detecta_EOF ) begin

			// Primera pregunta: REQA.
			if ( lectura_bit == INSTRUCCION_REQA && cuenta_bit == FINAL_INSTRUCCION_REQ1 && estado_picc != BLOQUEADO )
			begin										// No puede estar en estado BLOQUEADO.
				REQA_leido				<= 1'b1;
				instruccion_leida		<= 1'b1;
			end

			// Segunda pregunta: WUPA.
			else if ( lectura_bit == INSTRUCCION_WUPA && cuenta_bit == FINAL_INSTRUCCION_WUP1 ) // && estado_picc != ACTIVADO )
			begin
				REQA_leido				<= 1'b1;
				instruccion_leida		<= 1'b1;
			end

			// 3ra pregunta: SEL completo.
			else if (lectura_bit == INSTRUCCION_MAX && cuenta_bit == FINAL_INSTRUCCION_MAX2 && estado_picc==PREPARADO )
			begin
					enviar_SAK			<= 1'b1;
					instruccion_leida	<= 1'b1;
			end

			// 4ta pregunta: HALT completo.
			else if (lectura_bit == INSTRUCCION_HLTA && cuenta_bit == FINAL_INSTRUCCION_HLT1  && estado_picc == ACTIVADO )
				HLTA_leido              <= 1'b1;


			// Esto es nuevo 		************************************

			// Instruccion READ -> Cuenta hasta 36
			else if (lectura_bit[35:28] == INSTRUCCION_READ_X && cuenta_bit == FINAL_INSTRUCCION_READ1  && estado_picc == ACTIVADO )
			begin
				if (!(^lectura_bit)) begin
				// Son 4 bytes de informacion -> Al ser n� par, el XOR de todos sus bits debe dar 0 para cheaquear paridad
					{READ_x_leido, instruccion_leida}										<= 2'b11;
					{bloque_leido[0], bloque_leido[1], bloque_leido[2], bloque_leido[3]}	<= lectura_bit[26:23];
					CRC_leido																				<= {lectura_bit[17:10],lectura_bit[8:1]};
				end
				else begin
					{READ_x_leido, instruccion_leida, bloque_leido, CRC_leido}				<= 22'b0;
					enviar_NACK_paridad														<= 1'b1;
				end
			end
			// Instruccion READ -> Cuenta hasta 37
			else if (lectura_bit[36:29] == INSTRUCCION_READ_X && cuenta_bit == FINAL_INSTRUCCION_READ2  && estado_picc == ACTIVADO )
			begin
				if (!(^lectura_bit)) begin
				// Son 4 bytes de informacion -> Al ser n� par, el XOR de todos sus bits debe dar 0 para cheaquear paridad
					{READ_x_leido, instruccion_leida}										<= 2'b11;
					{bloque_leido[0], bloque_leido[1], bloque_leido[2], bloque_leido[3]}	<= lectura_bit[27:24];
					CRC_leido																<= {lectura_bit[18:11],lectura_bit[9:2]};
					enviar_NACK_paridad														<= 1'b0;
				end
				else begin
					{READ_x_leido, instruccion_leida, bloque_leido, CRC_leido}				<= 22'b0;
					enviar_NACK_paridad														<= 1'b1;
				end
			end
			// Instruccion WRITE -> Cuenta hasta 72
			else if (lectura_bit[71:64] == INSTRUCCION_WRITE && cuenta_bit == FINAL_INSTRUCCION_WRITE1  && estado_picc == ACTIVADO )
			begin
				if (!(^lectura_bit)) begin
				// Son 8 bytes de informacion -> Al ser n� par, el XOR de todos sus bits debe dar 0 para cheaquear paridad
					{WRITE_leido, instruccion_leida}										<= 2'b11;
					{bloque_leido[0], bloque_leido[1], bloque_leido[2], bloque_leido[3]}	<= lectura_bit[62:59];
					datos_WRITE																<= lectura_bit[53:18];
					CRC_leido																<= {lectura_bit[17:10],lectura_bit[8:1]};
					enviar_NACK_paridad														<= 1'b0;
				end
				else begin
					{WRITE_leido, instruccion_leida, bloque_leido, datos_WRITE, CRC_leido}	<=58'b0;
					enviar_NACK_paridad														<= 1'b1;
				end
			end
			// Instruccion WRITE -> Cuenta hasta 73
			else if (lectura_bit[72:65] == INSTRUCCION_WRITE && cuenta_bit == FINAL_INSTRUCCION_WRITE2  && estado_picc == ACTIVADO )
			begin
				if (!(^lectura_bit)) begin
				// Son 8 bytes de informacion -> Al ser n� par, el XOR de todos sus bits debe dar 0 para cheaquear paridad
					{WRITE_leido, instruccion_leida}										<= 2'b11;
					{bloque_leido[0], bloque_leido[1], bloque_leido[2], bloque_leido[3]}	<= lectura_bit[63:60];
					datos_WRITE																<= lectura_bit[54:19];
					CRC_leido																<= {lectura_bit[18:11],lectura_bit[9:2]};
				end
				else begin
					{WRITE_leido, instruccion_leida, bloque_leido, datos_WRITE, CRC_leido}	<=58'b0;
					enviar_NACK_paridad														<= 1'b1;
				end
			end
			// Hasta aca lo nuevo ****************************************

			// 5ta pregunta: SEL incompleto + anticolision
			else if ( cuenta_bit >= FINAL_MINIMO_ANTICOLISION  && estado_picc==PREPARADO && sel_detectado )
			begin
				if ( lectura_bit[0] && lectura_bit_desplazada == lectura_auxiliar_desplazada ) begin
					anticolision_activado	<= 1'b1;
					instruccion_leida		<= 1'b1;
				end
				else if ( !lectura_bit[0] && lectura_bit_desplazada == lectura_auxiliar_desplazada2 ) begin
					anticolision_activado	<= 1'b1;
					instruccion_leida		<= 1'b1;
				end
			end
		end	// Termina else if(detecta_EOF)


		else if (clk_traduccion) begin
			if (traduccion==2'b0) begin	// Corresponde al SOF (0 cero). No entra nunca ac�. Lo dejo xq tira warning.
				lectura_bit				<= VACIO_INSTRUCCION;
				cuenta_bit 				<= INICIAL_INSTRUCCION;
				lectura_auxiliar		<= VACIO_INSTRUCCION;
				lectura_completa		<= INSTRUCCION_MAX;
			end
			else if (traduccion==2'b11) begin	// Corresponde a "01"
				lectura_bit		[80:2]	<= lectura_bit[78:0];		// Desplazo los registros.
				lectura_bit		[1:0]	<= 2'b01;					// Guardo nuevos valores.
				lectura_auxiliar[80:2]	<= lectura_auxiliar[78:0];
				lectura_auxiliar[1]		<= lectura_completa[80];
				lectura_auxiliar[0]		<= lectura_completa[79];
				lectura_completa[80:2]	<= lectura_completa[78:0];
				lectura_completa[1:0]	<= 2'b00;
				cuenta_bit 				<= cuenta_bit + 7'b10;
			end
			else if (traduccion==2'b10) begin
				if (lectura_bit[0]) begin
					lectura_bit		[80:2]	<= lectura_bit[78:0];		// Desplazo los registros.
					lectura_bit		[1:0]	<= 2'b00;					// Guardo nuevos valores.
					cuenta_bit 				<= cuenta_bit + 7'b10;
					lectura_auxiliar[80:2]	<= lectura_auxiliar[78:0];
					lectura_auxiliar[1]		<= lectura_completa[80];
					lectura_auxiliar[0]		<= lectura_completa[79];
					lectura_completa[80:2]	<= lectura_completa[78:0];
					lectura_completa[1:0]	<= 2'b00;
				end
				else begin
					lectura_bit		[80:1]	<= lectura_bit[79:0];		// Desplazo los registros.
					lectura_bit		[0]		<= 1'b1;					// Guardo nuevo valor.
					cuenta_bit 				<= cuenta_bit + 7'b1;
					lectura_auxiliar[80:1]	<= lectura_auxiliar[79:0];
					lectura_auxiliar[0]		<= lectura_completa[80];
					lectura_completa[80:1]	<= lectura_completa[79:0];
					lectura_completa[0]		<= 1'b0;
				end
			end
			else if (traduccion==2'b01) begin
				lectura_bit		[80:1]  <= lectura_bit[79:0];	// Desplazo los registros.
				lectura_bit		[0]     <= lectura_bit[0];		// Guardo nuevo valor.
				cuenta_bit 				<= cuenta_bit + 7'b1;
				lectura_auxiliar[80:1]	<= lectura_auxiliar[79:0];
				lectura_auxiliar[0]		<= lectura_completa[80];
				lectura_completa[80:1]	<= lectura_completa[79:0];
				lectura_completa[0]		<= 1'b0;
			end
			lectura_bit_desplazada				<= lectura_bit				<< ( FINAL_INSTRUCCION_MAX2 - cuenta_bit + FINAL_MINIMO_ANTICOLISION);
			lectura_auxiliar_desplazada			<= lectura_auxiliar			<< ( FINAL_INSTRUCCION_MAX2 - cuenta_bit + FINAL_MINIMO_ANTICOLISION);
			lectura_auxiliar_desplazada2[80:1]	<= lectura_auxiliar[80:1]	<< ( FINAL_INSTRUCCION_MAX2 - cuenta_bit + FINAL_MINIMO_ANTICOLISION);
			lectura_auxiliar_desplazada2[0]		<= 1'b0;

			if ( lectura_bit == INSTRUCCION_SEL && cuenta_bit == FINAL_INSTRUCCION_SEL )
				sel_detectado	<= 1'b1;

		end	// Cierra IF del clock de traduccion.

	end		// Cierra always.

// *********************************|~~~~~~~~~~~|***************************************************************
// *********************************| 3ra Parte |***************************************************************
// *********************************|~~~~~~~~~~~|***************************************************************
// Esta parte se encarga de enviar la respuesta hacia el modulador dependiendo de lo que fue leida en la 2da parte.

	localparam INICIAL_RTA				= 8'b0;
	localparam FINAL_REQA				= 8'b0001_0001;	// Cuenta hasta 19. -> Pero env�o 18 bits. Esto es xq necesito un pulso m�s de clk_rta para actualizar el reg "modula".
	localparam FINAL_READ				= 8'b1010_0011;	// Cuenta hasta 163. -> Pero env�o 162 bits. Idem arriba.
	localparam FINAL_SAK					= 8'b0001_1100;	// Cuenta hasta 28. -> Pero env�o 27 bits. �dem arriba.
	localparam FINAL_ACK_NACK			= 8'b0000_0101;	// Cuenta hasta 5. Ver arriba
	localparam FINAL_ANTICOLISION2	= 8'b0100_0000;

	localparam bits_ATQA		= 18'b1000_0000_0_0000_0000_1;					// (0001h) de ATQA.
	localparam bits_READ_0 	= 162'b1000_0000_0_0100_0000_0_0010_0000_0_0001_0000_0_0000_0000_1_0000_0000_1_0000_0000_1_0000_0000_1_0000_0000_1_0000_0000_1_0000_0000_1_0000_0000_1_1000_0111_1_0000_1000_0_0110_0000_1_0000_0000_1_0110_1011_0_0000_1111_1;
			//   bits_READ_0	= (01 02 04 08 - 00 00 00 00 - 00 00 00 00 - E1 10 06 00)h + (D6 F0)h CRC
	localparam bits_READ_2 	= 162'b0000_0000_1_0000_0000_1_0000_0000_1_0000_0000_1_1000_0111_1_0000_1000_0_0110_0000_1_0000_0000_1_1100_0000_1_0000_0000_1_0111_1111_0_0000_0000_1_0000_0000_1_0000_0000_1_0000_0000_1_0000_0000_1_0010_1100_0_0010_1111_0;
			//   bits_READ_2	= (00 00 00 00 - E1 10 06 00 - 03 00 FE 00 - 00 00 00 00)h + (34 F4)h CRC
	localparam bits_READ_3 	= 162'b1000_0111_1_0000_1000_0_0110_0000_1_0000_0000_1_1100_0000_1_0000_0000_1_0111_1111_0_0000_0000_1_0000_0000_1_0000_0000_1_0000_0000_1_0000_0000_1_0000_0000_1_0000_0000_1_0000_0000_1_0000_0000_1_1110_0011_0_1001_1000_0;
			//   bits_READ_3	= (E1 10 06 00 - 03 00 FE 00 - 00 00 00 00 - 00 00 00 00)h + (C7 19)h CRC
	localparam bits_READ_4 	= 162'b1100_0000_1_0000_0000_1_0111_1111_0_0000_0000_1_0000_0000_1_0000_0000_1_0000_0000_1_0000_0000_1_0000_0000_1_0000_0000_1_0000_0000_1_0000_0000_1_0000_0000_1_0000_0000_1_0000_0000_1_0000_0000_1_1000_0011_0_0010_0001_1;
			//   bits_READ_4	= (03 00 FE 00 - 00 00 00 00 - 00 00 00 00 - 00 00 00 00)h + (C1 84)h CRC
	localparam bits_SAK		= 27'b0000_0000_1_0111_1111_0_1000_1010_0;		//  (00h) SAK + (FEh) CRC_1 + (51h) CRC_2
	localparam bits_ACK		= 4'b0101;		//  0xA
	localparam bits_NACK		= 4'b0000;		//  0xA

//  2 datos importantes en cuanto a lo enviado al codificador que luego envia al modulador:
// 1ro) El EOF no se modula asi que no se envia como dato.
// 2do) El SOF es un '1' que lo pone al inicio el codificador y lo envia. Por eso no forma parte de la trama de bits enviada.

	reg [7:0] 	cuenta_rta;
	reg [17:0] 	reg_REQA;
	reg [161:0] reg_READ_x;
	reg [3:0] 	reg_ACK;
	reg [3:0] 	reg_NACK;
	reg [26:0] 	reg_SAK;

	// Para Fc/4
	localparam INICIAL_DIVISOR_RTA	= 5'b0;
	localparam FINAL_DIVISOR_RTA	= 5'b1_1111;	// = 31
	localparam MITAD_DIVISOR_RTA	= 5'b0_1111;
	reg [4:0]  divisor_rta;
	reg 	   clk_rta;
	reg 	   salida_codificador;

// Este always funciona con un clock que se habilita siempre que se detecta un comenzar_escritura.
// El envio se hace con la cuenta de pulsos de clock que arranca solo cuando se activa "comenzar_escritura".
// Bit a bit se hace el envio. Cuando termina activa "rta_enviada".

	reg [44:0] reg_anticolision;

	always@(posedge portadora)
	begin: GENERADOR_BITS_RESPUESTA
		// Se resetea con una se�al que se activa cuando se habilita "comenzar_escritura".
			// Podria usar como reset "ultima_pausa" o "arranca_cuenta_demora"

		if( !habilita_escritura || reset_sinc ) begin
			salida_codificador	<= 1'b1;		// Es el valor del SOF
			cuenta_rta			<= INICIAL_RTA;
			reg_REQA			<= bits_ATQA;
			reg_ACK				<= bits_ACK;
			reg_NACK			<= bits_NACK;
			reg_READ_x[161:18]	<= READ_memoria;
			reg_READ_x[17:10]	<= registros_CRC[15:8];
			reg_READ_x[9]		<= registros_CRC[15]^registros_CRC[14]^registros_CRC[13]^registros_CRC[12]^registros_CRC[11]^registros_CRC[10]^registros_CRC[9]^registros_CRC[8];
			reg_READ_x[8:1]		<= registros_CRC[7:0];
			reg_READ_x[0]		<= registros_CRC[7]^registros_CRC[6]^registros_CRC[5]^registros_CRC[4]^registros_CRC[3]^registros_CRC[2]^registros_CRC[1]^registros_CRC[0];
			reg_anticolision	<= escritura_auxiliar;
			reg_SAK				<= bits_SAK;
			rta_enviada			<= 1'b0;
		end
		else if (clk_rta) begin
			if (manda_REQ) begin
				salida_codificador	<= reg_REQA[17];
				reg_REQA[17:1]		<= reg_REQA[16:0];
				if (cuenta_rta == FINAL_REQA)	begin
					rta_enviada	<= 1'b1;
					cuenta_rta	<= cuenta_rta;
				end
				else begin
					cuenta_rta	<= cuenta_rta + 8'b1;
					rta_enviada <= 1'b0;
				end
			end
			// Cuando se lee un READ se envian los bits de resupuesta si el CRC resulto OK o se envia NACK en caso contrario
			else if (manda_READ_x) begin
				if (CRC_corroborado_OK) begin
					salida_codificador	<= reg_READ_x[161];
					reg_READ_x[161:1]		<= reg_READ_x[160:0];
					if (cuenta_rta == FINAL_READ)	begin
						rta_enviada	<= 1'b1;
						cuenta_rta	<= cuenta_rta;
					end
					else begin
						cuenta_rta	<= cuenta_rta + 8'b1;
						rta_enviada <= 1'b0;
					end
				end
				else begin
					salida_codificador	<= reg_NACK[3];
					reg_NACK[3:1]		<= reg_NACK[2:0];
					reg_NACK[0]			<= 1'b1;	//	Esto lo puse xq sino tira warnings el programa.
					if (cuenta_rta == FINAL_ACK_NACK)	begin
						rta_enviada	<= 1'b1;
						cuenta_rta	<= cuenta_rta;
					end
					else begin
						cuenta_rta	<= cuenta_rta + 8'b1;
						rta_enviada <= 1'b0;
					end
				end
			end
			// Cuando se lee un WRITE se envia ACK si el CRC resulto OK o se envia NACK en caso contrario
			else if (manda_WRITE) begin
				if (CRC_corroborado_OK) begin
					salida_codificador	<= reg_ACK[3];
					reg_ACK[3:1]		<= reg_ACK[2:0];
					reg_ACK[0]			<= 1'b0;	//	Esto lo puse xq sino tira warnings el programa.
				end
				else begin
					salida_codificador	<= reg_NACK[3];
					reg_NACK[3:1]		<= reg_NACK[2:0];
					reg_NACK[0]			<= 1'b1;	//	Esto lo puse xq sino tira warnings el programa.
				end
				if (cuenta_rta == FINAL_ACK_NACK)	begin
					rta_enviada	<= 1'b1;
					cuenta_rta	<= cuenta_rta;
				end
				else begin
					cuenta_rta	<= cuenta_rta + 8'b1;
					rta_enviada <= 1'b0;
				end
			end
			// Este NACK se envia cuando hay problemas de paridad en la trama leida
			else if (manda_NACK) begin
				salida_codificador	<= reg_NACK[3];
				reg_NACK[3:1]		<= reg_NACK[2:0];
				reg_NACK[0]			<= 1'b1;	//	Esto lo puse xq sino tira warnings el programa.
				if (cuenta_rta == FINAL_ACK_NACK)	begin
					rta_enviada	<= 1'b1;
					cuenta_rta	<= cuenta_rta;
				end
				else begin
					cuenta_rta	<= cuenta_rta + 8'b1;
					rta_enviada <= 1'b0;
				end
			end
			else if (manda_anticolision) begin
				salida_codificador      <= reg_anticolision[44];
				reg_anticolision[44:1]  <= reg_anticolision[43:0];
				reg_anticolision[0]		<= 1'b1;	//	Esto lo puse xq sino tira warnings el programa
				if ( cuenta_rta+cuenta_bits_leidos == FINAL_ANTICOLISION2+extiende_cuenta ) begin
					rta_enviada <= 1'b1;
					cuenta_rta	<= cuenta_rta;
				end
				else begin
					cuenta_rta	<= cuenta_rta + 8'b1;
					rta_enviada <= 1'b0;
				end
			end
			else if (manda_SAK) begin
				salida_codificador 	<= reg_SAK[26];
				reg_SAK[26:1]      	<= reg_SAK[25:0];
				reg_SAK[0]			<= 1'b1;	//	Esto lo puse xq sino tira warnings el programa
				if (cuenta_rta == FINAL_SAK) begin
					rta_enviada <= 1'b1;
					cuenta_rta  <= cuenta_rta;
				end
				else begin
					cuenta_rta  <= cuenta_rta + 8'b1;
					rta_enviada <= 1'b0;
				end
			end    // Termina if del SAK.
		end// Termina elseif del clock_rta.
	end	// Termina always.

// Para Fc/4
	localparam FINAL_MODULADOR		= 2'b11;	// = 3
	localparam INICIAL_MODULADOR	= 2'b00;
	localparam MITAD_MODULADOR		= 2'b01;	// = 1
	reg [1:0]  divisor_modulador;   // Contador para calcular los 8 ciclos de portadora.
	reg 	   clk_modulador_2;
	reg 	   clk_modulador;

// Este always genera un clock (clk_rta) para contar el tiempo de envio entre cada uno de los bits.
// Ademas genera un clock (clk_modulador) que es el que usa como subportadora y para calcular la mitad del bit y asi poder invertir la modulacion.

	always@(posedge portadora)
	begin: DIVISOR_FRECUENCIA
		if ( !habilita_escritura || reset_sinc || rta_enviada ) begin
			clk_rta			<= 1'b1;
			divisor_rta		<= INICIAL_DIVISOR_RTA;
			clk_modulador	<= 1'b0;
		end
		else begin
			if ( divisor_rta == FINAL_DIVISOR_RTA ) begin
				clk_rta			<= 1'b1;
				clk_modulador	<= 1'b1;
				divisor_rta		<= INICIAL_DIVISOR_RTA;
			end
			else if ( divisor_rta == MITAD_DIVISOR_RTA ) begin
				clk_rta			<= clk_rta;
				clk_modulador	<= 1'b1;
				divisor_rta		<= divisor_rta + 5'b1;	// Para Fc/4
			end
			else begin
				divisor_rta		<= divisor_rta + 5'b1;	// Para Fc/4
				clk_rta			<= 1'b0;
				clk_modulador	<= 1'b0;
			end
		end	// Termina else de portadora.
	end

	always@(posedge portadora)
	begin: DIVISOR_MODULADOR2
 		case ( {habilita_escritura, reset_sinc, rta_enviada, divisor_modulador} )
			5'b10000:	{clk_modulador_2, divisor_modulador} <= {1'b0, 2'b01};
			5'b10001:	{clk_modulador_2, divisor_modulador} <= {1'b0, 2'b10};
			5'b10010:	{clk_modulador_2, divisor_modulador} <= {1'b1, 2'b11};
			5'b10011:	{clk_modulador_2, divisor_modulador} <= {1'b1, 2'b00};
			default:	{clk_modulador_2, divisor_modulador} <= {1'b0, 2'b00};
		endcase
	end


	reg modula;

//Este always genera la se�al de modulacion de subportadora a la mitad del bit dependiendo el valor de "salida_codificador".
//Este always cuenta los pulsos de "clk_modulador" para dividir el bit en 2 (parte modulada y sin modular).

	always@(posedge portadora)
	begin: CODIFICADOR
		if ( !habilita_escritura || reset_sinc || rta_enviada )
			modula				<= 1'b1;
		else if (clk_rta)
			modula	<= salida_codificador;
		else if (clk_modulador)
			modula	<= ~modula;
	end

	assign salida_modulador	= clk_modulador_2 & modula;		// Para Fc/4
	assign salida_auxiliar	= clk_modulador_2 & modula;		// Para Fc/4


	always@(posedge portadora)
	begin: ESTADOS_PICC

		case( {reset_sinc, HLTA_leido, rta_enviada, manda_REQ, manda_SAK, estado_picc} )

			7'b0_0000_00:	estado_picc	<= 2'b00;	// No hay nada leido.
			7'b0_0000_01:	estado_picc	<= 2'b01;	// No hay nada leido.
			7'b0_0000_10:	estado_picc	<= 2'b10;	// No hay nada leido.
			7'b0_0000_11:	estado_picc	<= 2'b11;	// No hay nada leido.

			7'b0_0001_00:	estado_picc	<= 2'b00;	// Todavia no se llego al final.
			7'b0_0001_01:	estado_picc	<= 2'b01;	// Todavia no se llego al final.
			7'b0_0001_10:	estado_picc	<= 2'b10;	// Todavia no se llego al final.
			7'b0_0001_11:	estado_picc	<= 2'b11;	// Todavia no se llego al final.

			7'b0_0010_00:	estado_picc	<= 2'b00;	// Todavia no se llego al final.
			7'b0_0010_01:	estado_picc	<= 2'b01;	// Todavia no se llego al final.
			7'b0_0010_10:	estado_picc	<= 2'b10;	// Todavia no se llego al final.
			7'b0_0010_11:	estado_picc	<= 2'b11;	// Todavia no se llego al final.

			//7'b0_0011_XX:	estado_picc	<= estado_picc;	// Error

			7'b0_0100_00:	estado_picc	<= 2'b00;	// Podr�a haberse enviado trama anticolision que no produce cambio de estado.
			7'b0_0100_01:	estado_picc	<= 2'b01;	// Podr�a haberse enviado trama anticolision que no produce cambio de estado.
			7'b0_0100_10:	estado_picc	<= 2'b10;	// Podr�a haberse enviado trama anticolision que no produce cambio de estado.
			7'b0_0100_11:	estado_picc	<= 2'b11;	// Podr�a haberse enviado trama anticolision que no produce cambio de estado.

			7'b0_0101_00:	estado_picc	<= ACTIVADO;
			7'b0_0101_01:	estado_picc	<= ACTIVADO;
			7'b0_0101_10:	estado_picc	<= ACTIVADO;
			7'b0_0101_11:	estado_picc	<= ACTIVADO;

			7'b0_0110_00:	estado_picc	<= PREPARADO;
			7'b0_0110_01:	estado_picc	<= PREPARADO;
			7'b0_0110_10:	estado_picc	<= PREPARADO;
			7'b0_0110_11:	estado_picc	<= PREPARADO;

			//7'b0_0111_XX:	estado_picc	<= estado_picc; // Error

			7'b0_1000_00:	estado_picc	<= BLOQUEADO;
			7'b0_1000_01:	estado_picc	<= BLOQUEADO;
			7'b0_1000_10:	estado_picc	<= BLOQUEADO;
			7'b0_1000_11:	estado_picc	<= BLOQUEADO;

			default:		estado_picc	<= OCIOSO;		// Si entra en un estado indefinido o de error vuelve al inicio.

		endcase
	end

	localparam BLOQUE_0			= 36'b1000_0000_0_0100_0000_0_0010_0000_0_0001_0000_0; // 32'h01_02_04_08
	localparam BLOQUE_1			= 36'b0000_0000_1_0000_0000_1_0000_0000_1_0000_0000_1; // 32'h0;
	localparam BLOQUE_2			= 36'b0000_0000_1_0000_0000_1_0000_0000_1_0000_0000_1; // 32'h0;
	localparam BLOQUE_3			= 36'b1000_0111_1_0000_1000_0_0110_0000_1_0000_0000_1; // 32'hE1_10_06_00;
	localparam BLOQUE_4			= 36'b1100_0000_1_0000_0000_1_0111_1111_0_0000_0000_1; // 32'h03_00_FE_00;
	localparam BLOQUES_5_15		= 396'h0;

	reg [35:0] 	memoria [0:15];
	reg [143:0] READ_memoria;
	reg [35:0] 	datos_WRITE_corroborando;
	reg [3:0] 	bloque_leido_corroborando;

	always@(posedge portadora)
	begin: MEMORIA_INTERNA

		if (reset_sinc) begin
			{memoria[0], memoria[1], memoria[2], memoria[3], memoria[4]} <= {BLOQUE_0, BLOQUE_1, BLOQUE_2, BLOQUE_3, BLOQUE_4};
			{memoria[5], memoria[6], memoria[7], memoria[8], memoria[9], memoria[10], memoria[11], memoria[12], memoria[13], memoria[14], memoria[15]} <= BLOQUES_5_15;
			READ_memoria[143:0]			<= 144'b0;
			datos_WRITE_corroborando	<= 36'b0;
			bloque_leido_corroborando	<= 4'b0;
		end

		else begin
			if (READ_x_leido) begin
				READ_memoria[143:0]	<= {memoria[bloque_leido], memoria[bloque_leido+1], memoria[bloque_leido+2], memoria[bloque_leido+3]};
			end
			else if (WRITE_leido) begin
				bloque_leido_corroborando	<= bloque_leido;
				datos_WRITE_corroborando	<= datos_WRITE;
			end
			else if (manda_WRITE && CRC_corroborado_OK && rta_enviada)
				memoria[bloque_leido_corroborando] <= datos_WRITE_corroborando;
			else begin
				READ_memoria				<= READ_memoria;
				datos_WRITE_corroborando	<= datos_WRITE_corroborando;
				{memoria[0], memoria[1], memoria[2], memoria[3], memoria[4], memoria[5]}	<= {memoria[0], memoria[1], memoria[2], memoria[3], memoria[4], memoria[5]};
				{memoria[6], memoria[7], memoria[8], memoria[9], memoria[10], memoria[11]}	<= {memoria[6], memoria[7], memoria[8], memoria[9], memoria[10], memoria[11]};
				{memoria[12], memoria[13], memoria[14], memoria[15]}	<= {memoria[12], memoria[13], memoria[14], memoria[15]};
			end
		end
	end
	
endmodule //end RFID module
