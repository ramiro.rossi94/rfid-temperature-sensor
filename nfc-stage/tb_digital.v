`timescale 1ns / 1ps

//////////////////////////////////////////////////////////////////////////////////

// Logica digital para transceptor de RFID
// Module Name:  digital_RFID
// Project Name: digital_RFID

// Ramiro Gonzalez
// UTN FRBA
// Abril 2017
//

//////////////////////////////////////////////////////////////////////////////////

module tb_digital;

   // Inputs
   reg entrada_muestra;
   wire portadora;
   reg resetn;
	//reg delta1;
	//reg delta2;

	// Outputs
	wire salida_modulador;
	wire salida_auxiliar;
//	wire leyo_READ_memoria_s;
//	wire CRC_corroborado_OK_s;
//	wire led_reset;
//	wire READ_x_leido_s;
//	wire [15:0] registros_CRC_s;
//	wire [15:0] entrada_CRC_2_s;
//	wire [15:0] registros_CRC_2_s;
//	wire [15:0] CRC_corroborar_s;
//	wire [15:0] CRC_leido_s;
//	wire [3:0] bloque_leido_s;
//	wire [127:0] entrada_CRC_s;
//	wire [143:0] READ_memoria_s;
	//wire [6:0] reg_nvb;

	// Internas
	reg reloj;
	reg portadora_aux;
	reg portadora_aux_2;
	
	assign portadora = reloj & ( (entrada_muestra ^ portadora_aux_2) | portadora_aux );
    

    // Instantiate the Unit Under Test (UUT)
    digital_RFID uut (
        .entrada_muestra(entrada_muestra), 
        .portadora(portadora), 
        .resetn(resetn),
		//.delta1(delta1),
		//.delta2(delta2),
        
			.salida_modulador(salida_modulador),
			.salida_auxiliar(salida_auxiliar)
//			.READ_x_leido_s(READ_x_leido_s),
//			.leyo_READ_memoria_s(leyo_READ_memoria_s),
//			.CRC_corroborado_OK_s(CRC_corroborado_OK_s),
//			.READ_memoria_s(READ_memoria_s),
//			.registros_CRC_s(registros_CRC_s),
//			.registros_CRC_2_s(registros_CRC_2_s),
//			.entrada_CRC_2_s(entrada_CRC_2_s),
//			.CRC_corroborar_s(CRC_corroborar_s),
//			.CRC_leido_s(CRC_leido_s),
//			.bloque_leido_s(bloque_leido_s),
//			.entrada_CRC_s(entrada_CRC_s),
//			.led_reset(led_reset)
		//.reg_nvb(reg_nvb)

    );

    always 
        #147  reloj =  ! reloj;

    initial begin
        // Initialize Inputs
        entrada_muestra	= 1;
		portadora_aux	= 0;
		portadora_aux_2	= 0;
        reloj			= 1;
        resetn			= 1;
		//delta1			= 0;
		//delta2			= 0;


        #37632;			// Espero 100ns hasta que activo el reset.
        resetn = 0;
		#9408;        // Wait 100 ns for global reset to finish.
		  
		  
// parameter INSTRUCCION_REQA      = 10'b0_0110_010_00; // = SOF (0) + REQA 010_0110 (26h) + 00 del EOF.        
        
        resetn = 1;
		#37632
		
        portadora_aux_2	= 1; #294 entrada_muestra = 0; portadora_aux_2 = 0;
        #1176 entrada_muestra = 0;
        #882 portadora_aux_2 = 1; #294 entrada_muestra = 1; portadora_aux_2 = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 2
        #882 portadora_aux_2 = 1; #294 entrada_muestra = 0; portadora_aux_2 = 0;
        #1176 entrada_muestra = 0;
        #882 portadora_aux_2 = 1; #294 entrada_muestra = 1; portadora_aux_2 = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 3
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #882 portadora_aux_2 = 1; #294 entrada_muestra = 0; portadora_aux_2 = 0;
        #1176 entrada_muestra = 0;
        #882 portadora_aux_2 = 1; #294 entrada_muestra = 1; portadora_aux_2 = 0;
        #1176 entrada_muestra = 1;
//BIT 4
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #882 portadora_aux_2 = 1; #294 entrada_muestra = 0; portadora_aux_2 = 0;
        #1176 entrada_muestra = 0;
        #882 portadora_aux_2 = 1; #294 entrada_muestra = 1; portadora_aux_2 = 0;
        #1176 entrada_muestra = 1;
//BIT 5
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 6
        #882 portadora_aux_2 = 1; #294 entrada_muestra = 0; portadora_aux_2 = 0;
        #1176 entrada_muestra = 0;
        #882 portadora_aux_2 = 1; #294 entrada_muestra = 1; portadora_aux_2 = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 7
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #882 portadora_aux_2 = 1; #294 entrada_muestra = 0; portadora_aux_2 = 0;
        #1176 entrada_muestra = 0;
        #882 portadora_aux_2 = 1; #294 entrada_muestra = 1; portadora_aux_2 = 0;
        #1176 entrada_muestra = 1;
//BIT 8
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 9
        #882 portadora_aux_2 = 1; #294 entrada_muestra = 0; portadora_aux_2 = 0;
        #1176 entrada_muestra = 0;
        #882 portadora_aux_2 = 1; #294 entrada_muestra = 1; portadora_aux_2 = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 10
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;

        //	#150528 ;
		//	#37632 ;
		#75264;
		portadora_aux	= 1;	// Activar esta linea para probar escritura con la entrada variando.
		  
//**************************************************************
//ESTA SECUENCIA ENVIA LA SE�AL REQA

//BIT 1
        #1176 entrada_muestra = 0;
		#1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 2
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 3
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 4
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 5
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;

//BIT 6
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 7
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 8
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 9
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 10
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
		  
		portadora_aux	= 0;
		// #75264;
		#150528;

//**************************************************************

// Esta secuencia env�a SEL con NVB m�nimo.
//BIT 1
        #1176 entrada_muestra = 0;
		#1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 2
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 3
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 4
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 5
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 6
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 7
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 8
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 9
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 10
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 11
        #1176 entrada_muestra = 1;
		#1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 12
        #1176 entrada_muestra = 0;
		#1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 13
        #1176 entrada_muestra = 0;
		#1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 14
        #1176 entrada_muestra = 0;
		#1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 15
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 16
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 17
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 18
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 19
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT EOF 1
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT EOF 2
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
		
		#602112;

//**************************************************************

// Esta secuencia envia Sel con NVB MAX (Todos los UID, etc)
//BIT 1
        #1176 entrada_muestra = 0;
		  #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 2
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 3
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 4
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 5
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 6
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 7
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 8
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 9
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 10
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;

//BIT 11 - Arranca NVB m�ximo
        #1176 entrada_muestra = 1;
		#1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 12
        #1176 entrada_muestra = 0;
		#1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 13
        #1176 entrada_muestra = 0;
		#1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 14
        #1176 entrada_muestra = 0;
		#1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 15
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 16
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 17
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 18
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 19 - Termina NVB m�ximo.
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;

/*
//BIT 11 - Arranca NVB variable. Recordar que viene de un 1 para simular bien el '0'.
        #1176 entrada_muestra = 1;
		#1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 12
        #1176 entrada_muestra = 0;
		#1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 13
        #1176 entrada_muestra = 0;
		#1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 14
        #1176 entrada_muestra = 0;
		#1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 15
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 16
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 17
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 18
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 19 - Termina NVB variable.
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
		
//BIT EOF 1
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT EOF 2
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
*/

//BIT 20 - Empieza UID0
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 21
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 22
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 23
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 24
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 25
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 26
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 27
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 28
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 29 - Empieza UID1
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 30
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 31
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 32
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 33
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 34
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 35
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 36
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 37
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 38 - Empieza UID2
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 39
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 40
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 41
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 42
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 43
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 44
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 45
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 46
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 47 - Empieza UID3
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 48
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 49
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 50
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 51
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 52
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 53
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 54
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 55
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 56 - Empieza BCC
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 57
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 58
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 59
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 60
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 61
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 62
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 63
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 64
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 65 - Empieza CRC1
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 66
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 67
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 68
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 69
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 70
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 71
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 72
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 73
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 74 - Empieza CRC2
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 75
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 76
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 77
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 78
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 79
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 80
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 81
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 82
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT - EOF 1
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT - EOF 2
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;

		#602112;
		
// Comienza secuencia Read para el bloque 5 = READ (30h) + BNO (00h) + CRC1 (02h) + CRC2 (A8h) = 

//BIT SOF
        #1176 entrada_muestra = 0;
		  #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 1 - Comienza READ
        #1176 entrada_muestra = 0;
		  #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 2
        #1176 entrada_muestra = 0;
		  #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 3
        #1176 entrada_muestra = 0;
		  #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 4
        #1176 entrada_muestra = 0;
		  #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 5
        #1176 entrada_muestra = 1;
		  #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 6
        #1176 entrada_muestra = 1;
		  #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 7
        #1176 entrada_muestra = 1;
		  #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 8
        #1176 entrada_muestra = 0;
		  #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 9
        #1176 entrada_muestra = 1;
		  #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 10 - Comienza BNO
        #1176 entrada_muestra = 1;
		  #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 11
        #1176 entrada_muestra = 0;
		  #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 12
        #1176 entrada_muestra = 0;
		  #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 13
        #1176 entrada_muestra = 0;
		  #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 14
        #1176 entrada_muestra = 0;
		  #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 15
        #1176 entrada_muestra = 0;
		  #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 16
        #1176 entrada_muestra = 0;
		  #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 17
        #1176 entrada_muestra = 0;
		  #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 18
        #1176 entrada_muestra = 1;
		  #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 19 - Comienza CRC1
        #1176 entrada_muestra = 1;
		  #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 20
        #1176 entrada_muestra = 1;
		  #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 21
        #1176 entrada_muestra = 1;
		  #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 22
        #1176 entrada_muestra = 0;
		  #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 24
        #1176 entrada_muestra = 0;
		  #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 24
        #1176 entrada_muestra = 0;
		  #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 25
        #1176 entrada_muestra = 0;
		  #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 26
        #1176 entrada_muestra = 0;
		  #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 27
        #1176 entrada_muestra = 0;
		  #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 28 - Comienza CRC2
        #1176 entrada_muestra = 0;
		  #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 29
        #1176 entrada_muestra = 0;
		  #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 30
        #1176 entrada_muestra = 0;
		  #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 31
        #1176 entrada_muestra = 1;
		  #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 32
        #1176 entrada_muestra = 1;
		  #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 33
        #1176 entrada_muestra = 1;
		  #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 34
        #1176 entrada_muestra = 1;
		  #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 35
        #1176 entrada_muestra = 1;
		  #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 36
        #1176 entrada_muestra = 1;
		  #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//EOF 1
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//EOF 2
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;

	#1806336;
	
	// Comienza secuencia Read para el bloque 5 = READ (30h) + BNO (05h) + CRC1 (AFh) + CRC2 (FFh) = 

//BIT SOF
        #1176 entrada_muestra = 0;
		  #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 1 - Comienza READ
        #1176 entrada_muestra = 0;
		  #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 2
        #1176 entrada_muestra = 0;
		  #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 3
        #1176 entrada_muestra = 0;
		  #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 4
        #1176 entrada_muestra = 0;
		  #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 5
        #1176 entrada_muestra = 1;
		  #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 6
        #1176 entrada_muestra = 1;
		  #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 7
        #1176 entrada_muestra = 1;
		  #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 8
        #1176 entrada_muestra = 0;
		  #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 9
        #1176 entrada_muestra = 1;
		  #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 10 - Comienza BNO
        #1176 entrada_muestra = 1;
		  #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 11
        #1176 entrada_muestra = 1;
		  #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 12
        #1176 entrada_muestra = 1;
		  #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 13
        #1176 entrada_muestra = 1;
		  #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 14
        #1176 entrada_muestra = 0;
		  #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 15
        #1176 entrada_muestra = 0;
		  #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 16
        #1176 entrada_muestra = 0;
		  #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 17
        #1176 entrada_muestra = 0;
		  #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 18
        #1176 entrada_muestra = 1;
		  #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 19 - Comienza CRC1
        #1176 entrada_muestra = 1;
		  #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 20
        #1176 entrada_muestra = 1;
		  #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 21
        #1176 entrada_muestra = 1;
		  #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 22
        #1176 entrada_muestra = 1;
		  #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 24
        #1176 entrada_muestra = 1;
		  #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 24
        #1176 entrada_muestra = 1;
		  #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 25
        #1176 entrada_muestra = 1;
		  #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 26
        #1176 entrada_muestra = 1;
		  #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 27
        #1176 entrada_muestra = 1;
		  #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 28 - Comienza CRC2
        #1176 entrada_muestra = 1;
		  #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 29
        #1176 entrada_muestra = 1;
		  #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 30
        #1176 entrada_muestra = 1;
		  #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 31
        #1176 entrada_muestra = 1;
		  #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 32
        #1176 entrada_muestra = 1;
		  #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 33
        #1176 entrada_muestra = 1;
		  #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 34
        #1176 entrada_muestra = 1;
		  #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 35
        #1176 entrada_muestra = 1;
		  #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 36
        #1176 entrada_muestra = 1;
		  #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//EOF 1
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//EOF 2
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;

	#1806336;	

// Comienza secuencia Read para el bloque 5 = WRITE (A2h) + BNO (05h) + Data1 (01h) + Data2 (02h) + Data3 (04h) + Data4 (08h) + CRC1 (58h) + CRC2 (DBh) = 

//BIT SOF
        #1176 entrada_muestra = 0;
		  #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 1 - Comienza WRITE
        #1176 entrada_muestra = 0;
		  #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 2
        #1176 entrada_muestra = 1;
		  #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 3
        #1176 entrada_muestra = 1;
		  #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 4
        #1176 entrada_muestra = 0;
		  #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 5
        #1176 entrada_muestra = 0;
		  #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 6
        #1176 entrada_muestra = 1;
		  #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 7
        #1176 entrada_muestra = 1;
		  #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 8
        #1176 entrada_muestra = 1;
		  #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 9
        #1176 entrada_muestra = 1;
		  #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 10 - Comienza BNO
        #1176 entrada_muestra = 1;
		  #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 11
        #1176 entrada_muestra = 1;
		  #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 12
        #1176 entrada_muestra = 1;
		  #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 13
        #1176 entrada_muestra = 1;
		  #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 14
        #1176 entrada_muestra = 0;
		  #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 15
        #1176 entrada_muestra = 0;
		  #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 16
        #1176 entrada_muestra = 0;
		  #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 17
        #1176 entrada_muestra = 0;
		  #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 18
        #1176 entrada_muestra = 1;
		  #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 19 - Comienza Data1
        #1176 entrada_muestra = 1;
		  #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 20
        #1176 entrada_muestra = 1;
		  #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 21
        #1176 entrada_muestra = 0;
		  #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 22
        #1176 entrada_muestra = 0;
		  #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 24
        #1176 entrada_muestra = 0;
		  #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 24
        #1176 entrada_muestra = 0;
		  #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 25
        #1176 entrada_muestra = 0;
		  #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 26
        #1176 entrada_muestra = 0;
		  #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 27
        #1176 entrada_muestra = 0;
		  #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 28 - Comienza Data2
        #1176 entrada_muestra = 0;
		  #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 29
        #1176 entrada_muestra = 1;
		  #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 30
        #1176 entrada_muestra = 1;
		  #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 31
        #1176 entrada_muestra = 0;
		  #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 32
        #1176 entrada_muestra = 0;
		  #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 33
        #1176 entrada_muestra = 0;
		  #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 34
        #1176 entrada_muestra = 0;
		  #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 35
        #1176 entrada_muestra = 0;
		  #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 36
        #1176 entrada_muestra = 0;
		  #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 37 - Comienza Data3
        #1176 entrada_muestra = 0;
		  #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 38
        #1176 entrada_muestra = 0;
		  #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 39
        #1176 entrada_muestra = 1;
		  #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 40
        #1176 entrada_muestra = 1;
		  #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 41
        #1176 entrada_muestra = 0;
		  #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 42
        #1176 entrada_muestra = 0;
		  #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 43
        #1176 entrada_muestra = 0;
		  #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 44
        #1176 entrada_muestra = 0;
		  #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 45
        #1176 entrada_muestra = 0;
		  #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 46 - Comienza Data4
        #1176 entrada_muestra = 0;
		  #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 47
        #1176 entrada_muestra = 0;
		  #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 48
        #1176 entrada_muestra = 0;
		  #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 49
        #1176 entrada_muestra = 1;
		  #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 50
        #1176 entrada_muestra = 1;
		  #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 51
        #1176 entrada_muestra = 0;
		  #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 52
        #1176 entrada_muestra = 0;
		  #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 53
        #1176 entrada_muestra = 0;
		  #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 54
        #1176 entrada_muestra = 0;
		  #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 55 - Comienza CRC1
        #1176 entrada_muestra = 0;
		  #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 56
        #1176 entrada_muestra = 0;
		  #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 57
        #1176 entrada_muestra = 0;
		  #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 58
        #1176 entrada_muestra = 1;
		  #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 59
        #1176 entrada_muestra = 1;
		  #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 60
        #1176 entrada_muestra = 1;
		  #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 61
        #1176 entrada_muestra = 1;
		  #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 62
        #1176 entrada_muestra = 1;
		  #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 63
        #1176 entrada_muestra = 0;
		  #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 64 - Comienza CRC2
        #1176 entrada_muestra = 1;
		  #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 65
        #1176 entrada_muestra = 1;
		  #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 66
        #1176 entrada_muestra = 1;
		  #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 67
        #1176 entrada_muestra = 1;
		  #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 68
        #1176 entrada_muestra = 1;
		  #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 69
        #1176 entrada_muestra = 1;
		  #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 70
        #1176 entrada_muestra = 1;
		  #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 71
        #1176 entrada_muestra = 1;
		  #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 72
        #1176 entrada_muestra = 1;
		  #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//EOF 1
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//EOF 2
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;

	#1806336
	
	// Comienza secuencia Read para el bloque 5 = READ (30h) + BNO (05h) + CRC1 (AFh) + CRC2 (FFh) = 

//BIT SOF
        #1176 entrada_muestra = 0;
		  #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 1 - Comienza READ
        #1176 entrada_muestra = 0;
		  #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 2
        #1176 entrada_muestra = 0;
		  #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 3
        #1176 entrada_muestra = 0;
		  #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 4
        #1176 entrada_muestra = 0;
		  #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 5
        #1176 entrada_muestra = 1;
		  #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 6
        #1176 entrada_muestra = 1;
		  #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 7
        #1176 entrada_muestra = 1;
		  #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 8
        #1176 entrada_muestra = 0;
		  #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 9
        #1176 entrada_muestra = 1;
		  #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 10 - Comienza BNO
        #1176 entrada_muestra = 1;
		  #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 11
        #1176 entrada_muestra = 1;
		  #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 12
        #1176 entrada_muestra = 1;
		  #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 13
        #1176 entrada_muestra = 1;
		  #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 14
        #1176 entrada_muestra = 0;
		  #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 15
        #1176 entrada_muestra = 0;
		  #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 16
        #1176 entrada_muestra = 0;
		  #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 17
        #1176 entrada_muestra = 0;
		  #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 18
        #1176 entrada_muestra = 1;
		  #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 19 - Comienza CRC1
        #1176 entrada_muestra = 1;
		  #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 20
        #1176 entrada_muestra = 1;
		  #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 21
        #1176 entrada_muestra = 1;
		  #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 22
        #1176 entrada_muestra = 1;
		  #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 24
        #1176 entrada_muestra = 1;
		  #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 24
        #1176 entrada_muestra = 1;
		  #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 25
        #1176 entrada_muestra = 1;
		  #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 26
        #1176 entrada_muestra = 1;
		  #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 27
        #1176 entrada_muestra = 1;
		  #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 28 - Comienza CRC2
        #1176 entrada_muestra = 1;
		  #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 0;//00
        #1176 entrada_muestra = 0;//00
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 29
        #1176 entrada_muestra = 1;
		  #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 30
        #1176 entrada_muestra = 1;
		  #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 31
        #1176 entrada_muestra = 1;
		  #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 32
        #1176 entrada_muestra = 1;
		  #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 33
        #1176 entrada_muestra = 1;
		  #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 34
        #1176 entrada_muestra = 1;
		  #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 35
        #1176 entrada_muestra = 1;
		  #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 36
        #1176 entrada_muestra = 1;
		  #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//EOF 1
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//EOF 2
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;

	#1806336;

	#2 $stop;

/*
// Esta secuencia envia HLTA.
//BIT SOF
        #1176 entrada_muestra = 0;
		#1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 1
        #1176 entrada_muestra = 0;
		#1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 2
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 3
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 4
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 5
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 6
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 7
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 8
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 9 - Bit de paridad del 1 byte.
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 10 - Arranca 2do byte transmitido.
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 11
        #1176 entrada_muestra = 0;
		#1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 12
        #1176 entrada_muestra = 0;
		#1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 13
        #1176 entrada_muestra = 0;
		#1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 14
        #1176 entrada_muestra = 0;
		#1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 15
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 16
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 17
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 18 - 2do bit de paridad
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 19 - Arranca 3er byte.
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 20
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 21
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 22
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 23
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 24
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 25
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 26
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 27 - 3er bit de paridad.
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 28 - Arranca 4to byte.
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 29
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 30
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 31
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 32
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 33
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 34
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 35
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 36 . 4to bit de paridad
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT EOF 1
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT EOF 2
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;

        #301056;
//**************************************************************
//ESTA SECUENCIA ENVIA LA SE�AL REQA

//BIT SOF
        #1176 entrada_muestra = 0;
		#1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 1
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 2
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 3
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 4
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;

//BIT 5
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 6
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 7
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT EOF 1
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT EOF 2
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;

		#301056;
		
//**************************************************************
//ESTA SECUENCIA ENVIA LA SE�AL WUPA

//BIT SOF
        #1176 entrada_muestra = 0;
		#1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 1
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 2
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 3
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 4
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;

//BIT 5
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 6
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT 7
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 0;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT EOF 1
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
//BIT EOF 2
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;
        #1176 entrada_muestra = 1;

		#301056 $stop;
*/
    end
      
endmodule
